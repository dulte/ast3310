import numpy as np
import matplotlib.pyplot as plt

class Convection2D:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.dx = (x_interval[1]-x_interval[0])/Nx
        self.dy = (y_interval[1]-y_interval[0])/Ny
                  
        
        ### Physical Constants ###

        self.mu = 0.61
        self.unit_mass = 1.661e-27
        self.gamma = 7./3
        self.G = 6.674e-11
        self.g = self.G*1.989e30/(695508e3)**2
        self.k_B = 1.381e-23
        
        ### Energy and temperature at top and bottom ###

        self.T_top = 5772
        self.T_bottom = 5772

        self.inital_energy_top = None
        self.inital_energy_bottom = None
        
        ### dt ###
        
        self.dt = None
        
        ### Nabla, the unstability criterion ###
        
        self.unsability_criterion = 2/5. + 0.1


        ### Variables for derivatives ###
        self.drho_dx = None
        self.drho_dy = None
        self.drho_dy = None

        self.de_dx = None
        self.de_dy = None
        self.de_dt = None

        self.dux_dx = None
        self.dux_dy = None
        self.dux_dt = None

        self.duy_dx = None
        self.duy_dy = None
        self.duy_dt = None

        self.deux_dx = None
        self.deuy_dy = None

        self.drhoux_dx = None
        self.drhouy_dy = None

        
    """ Functions for converting some of the quantities """
    def get_pressure(self,energy):
        return 1./(self.gamma-1)*energy

    def get_energy_from_temperature(self,T,density):
        return (self.gamma-1)*density/(self.mu*self.unit_mass)*self.k_B*T

    def get_temperature(self,energy,density):
        return (self.mu*self.unit_mass)*energy/((self.gamma-1)*density*self.k_B)
    
    
    """ Functions for differentiations in x and y """
    def dV_dx(self,V):
        return (np.roll(V,-1,axis=0) - np.roll(V,1,axis=0))/(2*self.dx)

    def dV_dy(self,V):
        dVdy = np.zeros_like(V)

        dVdy[:,1:-1] = ((np.roll(V,-1,axis=1) - np.roll(V,1,axis=1))/(2*self.dy))[:,1:-1]
        dVdy[:,0] = (-V[:,2] +4*V[:,1]-3*V[:,0])/(2*self.dy)
        dVdy[:,-1] = -(-V[:,-3] +4*V[:,-2]-3*V[:,-1])/(2*self.dy)
        return dVdy

    """ Function for mean of neighbours """
    def get_midpoint(self,V):
        """
        Given a variable V, this gets the mean of the neighbouring points
        """
        midpoint = 1./8*(np.roll(V,1,axis=0) + np.roll(V,-1,axis=0) + np.roll(V,1,axis=1) + np.roll(V,-1,axis=1)+\
                        np.roll(V,(1,1),axis=(0,1)) + np.roll(V,(1,-1),axis=(0,1)) + np.roll(V,(-1,1),axis=(0,1))\
                               + np.roll(V,(-1,-1),axis=(0,1)))
        #midpoint = 0.25*(np.roll(V,1,axis=0) + np.roll(V,-1,axis=0) + np.roll(V,1,axis=1) + np.roll(V,-1,axis=1)) #For only using 4 neighbors
        midpoint[:,0] = 0.5*(np.roll(V[:,0],1,axis=0) + np.roll(V[:,0],-1,axis=0))
        midpoint[:,-1] = 0.5*(np.roll(V[:,0],1,axis=0) + np.roll(V[:,0],-1,axis=0))
        return midpoint

    
    """ Functions for updating derivatives """
    def update_position_derivatives(self,u_x,u_y,density,energy):
        """
        Updates all the spacial derivatives.
        """
        self.drho_dx = self.dV_dx(density)
        self.drho_dy = self.dV_dy(density)

        self.de_dx = self.dV_dx(energy)
        self.de_dy = self.dV_dy(energy)

        self.dux_dx = self.dV_dx(u_x)
        self.dux_dy = self.dV_dy(u_x)
        self.dux_dy[:,0] = 0; self.dux_dy[:,-1] = 0

        self.duy_dx = self.dV_dx(u_y)
        self.duy_dy = self.dV_dy(u_y)

        self.deux_dx = self.dV_dx(u_x*energy)
        self.deuy_dy = self.dV_dy(u_y*energy)

        self.drhoux_dx = self.dV_dx(u_x*density)
        self.drhouy_dy = self.dV_dy(u_y*density)



    def update_time_derivatives(self,u_x,u_y,density,energy):
        """
        Updates all the time derivatives.
        """
        self.get_de_dt(u_x,u_y,density,energy)
        self.get_drho_dt(u_x,u_y,density,energy)
        self.get_dux_dt(u_x,u_y,density,energy)
        self.get_duy_dt(u_x,u_y,density,energy)

    def update_all_derivatives(self,u_x,u_y,density,energy):
        """
        Updates all the spacial derivatives by calling the functions
        for spacial and time derivatives.
        """
        self.update_position_derivatives(u_x,u_y,density,energy)
        self.update_time_derivatives(u_x,u_y,density,energy)


    """ Functions for calculating the indvidual time derivatives """
    def get_de_dt(self,u_x,u_y,density,energy):
        P = self.get_pressure(energy)
        self.de_dt = -self.deux_dx - self.deuy_dy - P*(self.dux_dx + self.duy_dy)
        
        #For using the non-working boundary condition for de/dt
        #self.de_dt = self.get_boundary_de_dt(self.de_dt,energy)


    def get_drho_dt(self,u_x,u_y,density,energy):
        self.drho_dt = -self.drhoux_dx - self.drhouy_dy

    def get_dux_dt(self,u_x,u_y,density,energy):
        self.dux_dt = -u_x*self.dux_dx - u_y*self.dux_dy - 1./(density*(self.gamma-1))*self.de_dx

    def get_duy_dt(self,u_x,u_y,density,energy):
        self.duy_dt = -u_x*self.duy_dx - u_y*self.duy_dy - 1./(density*(self.gamma-1))*self.de_dy + self.g
                                                              
                                                              
    
    """ Functions for stepping the indvidual variables """
    def get_next_energy(self,u_x,u_y,density,energy):
        midpoint = self.get_midpoint(energy)
        next_energy =  midpoint + self.de_dt*self.dt
        next_energy = self.get_boundary_energy(next_energy,density)
        return next_energy

    def get_next_density(self,u_x,u_y,density,energy):
        midpoint = self.get_midpoint(density)
        next_density =  midpoint + self.drho_dt*self.dt
        next_density = self.get_boundary_density(next_density,energy)
        return next_density

    def get_next_u_x(self,u_x,u_y,density,energy):
        midpoint = self.get_midpoint(u_x)
        next_ux =  midpoint + self.dux_dt*self.dt

        return next_ux

    def get_next_u_y(self,u_x,u_y,density,energy):
        midpoint = self.get_midpoint(u_y)
        next_uy =  midpoint + self.duy_dt*self.dt
        next_uy[:,0] = 0
        next_uy[:,-1] = 0
        return next_uy
    
    """ Functions for boundaries for energy and density """

    def get_boundary_de_dt(self,de_dt,energy):
        delta = 20
        de_dt[:,0] = -(energy[:,0] - self.inital_energy_top*0.9)/delta
        de_dt[:,-1] = -(energy[:,-1] - self.inital_energy_bottom*1.1)/delta
        return de_dt

    def get_boundary_energy(self,energy,density):

        T = self.T_top*0.90
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,0] = 1./(2*self.dy/temp_factor + 3)*(4*energy[:,1] - energy[:,2])

        T = self.T_bottom*1.1
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,-1] = 1./(2*self.dy/temp_factor + 3)*(4*energy[:,-2] - energy[:,-3])


        return energy

    def get_boundary_density(self,density,energy):

        
        #The three commented lines are the unused boundary conditions for density
        #when using boundary conditions on de/dt instead of e
        #density[:,0] = 1/((self.gamma - 1)*self.g)*self.de_dy[:,0] 
        #density[:,-1] = 1/((self.gamma - 1)*self.g)*self.de_dy[:,-1]
        #return density


        T = self.T_top*0.9
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,0] = temp_factor*energy[:,0]

        T = self.T_bottom*1.1
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,-1] = temp_factor*energy[:,-1]

        return density

    

    """ Function for distributing initial conditions """
    def distribute_initial_conditions_solar_surface(self,P_top,T_top):
        """
        Given the pressure and temperature of the outer photosphere of the sun
        this will distribute initial conditions according to the criteria
        found in the report.
        """
        self.T_top = T_top
        density = np.zeros((self.Nx,self.Ny))
        energy = np.zeros_like(density) 
        u_x = np.zeros_like(density) #+ np.random.normal(0,100,(self.Nx,self.Ny))
        u_y = np.zeros_like(density)
        pressure = np.zeros_like(density)
        temperature = np.zeros_like(density)

        temp_factor = self.mu*self.unit_mass/self.k_B

        pressure[:,0] = P_top
        temperature[:,0] = T_top
        energy[:,0] = (self.gamma - 1)*pressure[:,0]
        density[:,0] = temp_factor*pressure[:,0]/temperature[:,0]

        for i in range(1,self.Ny):
           pressure[:,i] = pressure[:,i-1] + self.g*self.dy*density[:,i-1]
           temperature[:,i] = temperature[:,i-1] + self.unsability_criterion*self.dy*self.g*\
                                        density[:,i-1]*temperature[:,i-1]/pressure[:,i-1]

           energy[:,i] = (self.gamma - 1)*pressure[:,i]
           density[:,i] = temp_factor*pressure[:,i]/temperature[:,i]


        #Boxes for kickstarting the convecton
        energy[20:40,40:60] *=10.0
        energy[260:280,40:60] *=10.0
              
              
              
        self.T_bottom = temperature[-1,-1]
        self.inital_energy_top = energy[0,0]
        self.inital_energy_bottom = energy[0,-1]
        return u_x,u_y,density,energy


    """ Function for tuning dt """
    def tune_dt(self,u_x,u_y,density,energy,p=0.1):
        """
        This function will tune dt given the variables and their derivatives.
        It will also ensure that dt neither becomes too large, or too small.
        It delta = 0, then the new dt will not be calculated, but set as a
        default 0.1. p=0.1 by default.
        """
        de_dt = self.de_dt
        drho_dt = self.drho_dt

        delta = [np.nanmax(np.abs(de_dt/energy)),np.nanmax(np.abs(drho_dt/density)),np.nanmax(np.abs(u_x/self.dx)),np.nanmax(np.abs(u_y/self.dy))]
        delta = np.nanmax(np.array(delta))

        
        """Sets a default dt if p=0"""
        if delta != 0:
            self.dt = p/delta
        else:
            self.dt = 0.1

        
        """Caps for the dt"""
        if self.dt>1:
            self.dt = 1
        elif self.dt < 0.001:
            self.dt = 0.001


    """ Function for testing spacial derivatives """
    def test_differentiation(self):
        """
        This function will make a sin function first in the x direction, and
        then y, and test the differentiation in these directions, and plot them
        agains the known analytical results.
        """
        
        grid = np.zeros((100,100))
        for i in range(100):
            grid[:,i] = np.sin(np.linspace(0,4*np.pi,100,endpoint=False))

        diff_grid = self.dV_dx(grid)
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),grid[:,0],label="Sin")
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),diff_grid[:,0],label="Cos")
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),np.cos(np.linspace(0,4*np.pi,100,endpoint=False)),label="Real Cos")
        plt.legend()
        plt.show()

        grid[:,:] = np.zeros((100,100))
        for i in range(100):
            grid[i,:] = np.sin(np.linspace(0,4*np.pi,100,endpoint=False))

        diff_grid = self.dV_dy(grid)
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),grid[0,:],label="Sin")
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),diff_grid[0,:],label="Cos")
        plt.plot(np.linspace(0,4*np.pi,100,endpoint=False),np.cos(np.linspace(0,4*np.pi,100,endpoint=False)),label="Real Cos")
        plt.legend()
        plt.show()


if __name__=="__main__":
    con = Convection2D(100,100,(0,4*np.pi),(0,4*np.pi))
    con.test_differentiation()
