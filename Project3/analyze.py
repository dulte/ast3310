# -*- coding: utf-8 -*-
"""
Created on Fri May 18 18:47:38 2018

@author: dulte
"""

import numpy as np
import matplotlib.pyplot as plt

mu = 0.61
unit_mass = 1.661e-27
gamma = 7./3#1.4
G = 6.674e-11
g = G*1.989e30/(695508e3)**2
k_B = 1.381e-23

cp = 5/2.*(k_B/(mu*unit_mass))

def dV_dx(V,dx):
    return (np.roll(V,-1,axis=0) - np.roll(V,1,axis=0))/(2*dx)

def dV_dy(V,dy):
    dVdy = np.zeros_like(V)
    dVdy[:,1:-1] = ((np.roll(V,-1,axis=1) - np.roll(V,1,axis=1))/(2*dy))[:,1:-1]
    dVdy[:,0] = (-V[:,2] +4*V[:,1]-3*V[:,0])/(2*dy)
    dVdy[:,-1] = -(-V[:,-3] +4*V[:,-2]-3*V[:,-1])/(2*dy)
    return dVdy


def load_data():
    pressure = np.load("pressure.npy")
    temperature = np.load("temperature.npy")
    density = np.load("density.npy")
    energy = np.load("energy.npy")
    u_x = np.load("ux.npy")
    u_y = np.load("uy.npy")

    return  pressure, temperature, density, energy, u_x, u_y

def get_H_p(pressure,dy):
    dydP = 1./(dV_dy(pressure,dy))
    return -pressure*dydP

def get_nabla(H_p,temperature,dy):
    
    dTdy = dV_dy(temperature,dy)
    return -H_p/temperature*dTdy

def get_Fc(dy,uy,energy):
    dx = abs(dy)
    Fc = np.zeros_like(energy[0,:])
    for i in range(energy.shape[1]):
        Fc[i] = np.sum(energy[:,i]*uy[:,i]*dx)

    return Fc

def get_delta_T(u_y,temperature):
    dT = np.zeros_like(u_y[0,:])
    temp_down = np.where(u_y<0,temperature,0)
    temp_up = np.where(u_y>0,temperature,0)
    
    dT = np.mean(temp_up-temp_down,axis=0)
    
    return dT


def get_convection_velocity(dy,density):
 
    drhody = dV_dy(density,dy)
 
    return np.sqrt(g*dy**2/density*drhody)
 
def get_nabla_difference(Hp,dT,temperature,dy):
    """
    The minus sign is duw to Hp being calculated with the box upside down.
    """
    temp_along_x = np.mean(temperature,axis=0)
    Hp_along_x = np.mean(Hp,axis=0)
    return -(dT*Hp_along_x)/(temp_along_x*dy)
 
def get_mixing_length(Fc,Hp,nabla_diff,density,temperature,v):
    temp_along_x = np.mean(temperature,axis=0)
    Hp_along_x = np.mean(Hp,axis=0)
    rho_along_x = np.mean(density,axis=0)
    v_along_x = np.mean(v,axis=0)
    return 2*Fc*Hp_along_x/(nabla_diff*rho_along_x*temp_along_x*cp*v_along_x)

if __name__=="__main__":
    dy = 40000
    pressure, temperature, density, energy, u_x, u_y = load_data()

    H_p = get_H_p(pressure,dy)
    nabla = get_nabla(H_p,temperature,dy)
    Fc = get_Fc(dy,-u_y,energy)
    v = get_convection_velocity(dy,density)
    dT = get_delta_T(-u_y, temperature)
    nabla_diff = get_nabla_difference(H_p,dT,temperature,dy)
    lm = get_mixing_length(Fc,H_p,nabla_diff,density,temperature,v)
    
    height = np.linspace(-1,0,100)
    plt.plot(height,np.flip(Fc,0))
    plt.xlim(height[0],height[-1])
    plt.title(r"Convective Flux $F_C$")
    plt.xlabel(r"Depth in percent of $4\cdot 10^6$")
    plt.ylabel(r"$F_c$")
    plt.show()
    
    print("Convective Velcocity: ",np.nanmax(v),np.nanmin(v))
    print("Nabla: ",np.nanmax(nabla),np.nanmin(nabla))
    print("Places with nabla > 2/5: ",np.sum(np.where(nabla>2/5.,1,0))/nabla.size)

    print("Mean y velocity", np.mean(-u_y))
    
    u_index = np.unravel_index((np.nanargmin(np.abs(v-(-u_y)))),u_y.shape)
    print("Where v is closest to u_y, the difference is: ", np.nanmin(np.abs(v-(-u_y)))/np.abs(u_y[u_index]) )
    print("This happens at height index: ",u_index[1])
    print("With v and u_y:", v[u_index],-u_y[u_index])
    
    plt.plot(height,np.flip(dT,0))
    plt.xlim(height[0],height[-1])
    plt.title(r"Temperature Difference")
    plt.xlabel(r"Depth in percent of $4\cdot 10^6$")
    plt.ylabel(r"$\Delta T$")
    plt.show()
    
    plt.plot(height,np.flip(nabla_diff,0))
    plt.xlim(height[0],height[-1])
    plt.title(r"Calculated $(\nabla^* - \nabla _p)$")
    plt.xlabel(r"Depth in percent of $4\cdot 10^6$")
    plt.ylabel(r"$(\nabla^* - \nabla _p)$")
    plt.show()
    
    plt.plot(height,np.flip(lm,0))
    plt.xlim(height[0],height[-1])
    plt.title(r"Mixing Length $l_m$")
    plt.xlabel(r"Depth in percent of $4\cdot 10^6$")
    plt.ylabel(r"$l_m$")
    plt.show()
    
    

