\documentclass[a4paper,norsk, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage[norsk]{babel}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{float}
\usepackage{amssymb}
\usepackage[dvips]{epsfig}
\usepackage[toc,page]{appendix}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}
\usepackage{titling}
\usepackage{marvosym }
%\usepackage{subcaption}
\usepackage[noabbrev]{cleveref}
\usepackage{cite}
\usepackage{subfig}
\usepackage{graphicx}
\usepackage[margin=1in]{geometry}


\setlength{\droptitle}{-10em}   % This is your set screw

\setcounter{tocdepth}{2}

\lstset{language=c++}
\lstset{alsolanguage=[90]Fortran}
\lstset{alsolanguage=Python}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\title{AST3310 Project 3}
\author{Daniel Heinesen, daniehei}
\begin{document}
\maketitle
\section{Introduction}
The goal in this project is to simulate the convection in the outer layer of a star i 2 dimensions. We will do this using the continuity equation and the equations of motion for momentum and energy. We will solve these equations numerically using a 2D grid and a diverse set of differentiation and integration schemes. By heating the bottom and cooling the top of our 2D box we hope to induce convection, and hopefully be able to more precisely calculate some of the parameters we used to describe convection in the last project.

\section{Theory}
\subsection{Equations of motion}\label{sec:eq_motion}
We are going to simulate a fluid -- more precise a ideal gas --, so we need to know the kind of equation that govern them. We are interested in describing the fluid with the following parameter: Density $\rho$, horizontal velocity $u_x$, vertical velocity $u_y$ and energy $e$. We know from fluid dynamics that the density is described by the continuity equation
\begin{equation}\label{eq:drho_dt}
\frac{\partial \rho}{\partial t} = -\frac{\partial \rho u_i}{\partial x_i} = -\frac{\partial \rho u_x}{\partial x} -\frac{\partial \rho u_y}{\partial y}
\end{equation}
We could expand the differentiations using the product rule, but we will use it as it stands here. This equation describes that the only way for a density inside an area to change with time is for either (1) the density outside the area has to be different from inside the area, or (2) the velocity outside is different from that inside the area. Quite similarly, the energy is described as follows:
\begin{equation}\label{eq:de_dt}
\frac{\partial e}{\partial t}  = -\frac{\partial e u_x}{\partial x} -\frac{\partial e u_y}{\partial y} - P\left(\frac{\partial u_x}{\partial x} +\frac{\partial u_y}{\partial y}\right).
\end{equation}
As with the density, we will calculate the derivatives of $e u_i$ directly instead of expanding them. The same logic that applies to the change of density in an area, applies to the change in energy, but we see that we an extra term which corresponds to the increase in internal energy if we have a flow in to the area -- the internal energy is closely related to the pressure --. We will see below how to obtain the pressure used here.

The two last parameters we were looking to simulate were the velocities, $u_x$ and $u_y$. From fluid dynamics we do not have equations of motion for these parameters, but instead for the momentums:
\begin{equation}
\frac{\partial \rho u_i}{\partial t} = -\frac{\partial }{\partial x_k}\left(\rho u_i u_k + \rho\langle w_iw_k\rangle\right).
\end{equation}
Here $\rho\langle w_iw_k\rangle$ is the so called stress tensor and describes the stress of the fluid due to strain rate. The stress tensor comprises of two parts
\begin{equation}
\rho\langle w_iw_k\rangle = P\delta_{ik} - \tau_{ik},
\end{equation}
where $\tau_{ik}$ is the viscous stress and is set to $0$ in our simulation, and $P\delta_{ik}$ reduces to the thermal pressure. $\delta_{ik}$ is simply the Kronecker delta. So the equation of motion for momentum reduces to
\begin{equation}
\frac{\partial \rho u_i}{\partial t} = -\frac{\partial }{\partial x_k}\left(\rho u_i u_k + P\delta_{ik}\right).
\end{equation}
We may add a force term to the end of this equation, something we have to do for the momentum in the $y$-direction since we are taking gravity into account. This force term is $\mathcal{F} = -g\rho$\footnote{Taken that the y-axis points upwards.}, where $g = \frac{GM_{\odot}}{R_{\odot}^2}=$constant. But we want equations for the velocities. For this we are rewrite the momentum equations, starting with the x-momentum:
\begin{equation}
\frac{\partial \rho u_x}{\partial t} = u_x\frac{\partial \rho}{\partial t} + \rho\frac{\partial u_x}{\partial t}  = -\frac{\partial }{\partial x}\rho u_x u_x -\frac{\partial }{\partial y}\rho u_x u_y - \frac{\partial }{\partial x}P 
\end{equation}
\begin{equation}
= -u_x\frac{\partial }{\partial x}\rho  u_x - \rho u_x\frac{\partial }{\partial x}u_x  - \rho u_y\frac{\partial }{\partial y}u_x -u_x\frac{\partial }{\partial y}\rho u_y  - \frac{\partial }{\partial x}P
\end{equation}
\begin{equation}
= - \rho u_x\frac{\partial }{\partial x}u_x  - \rho u_y\frac{\partial }{\partial y}u_x - \frac{\partial }{\partial x}P + u_x \underbrace{\left[ -\frac{\partial }{\partial x}\rho  u_x - \frac{\partial }{\partial y}\rho u_y\right]}_{\partial \rho/\partial t}.
\end{equation}
This gives us that
\begin{equation}\label{eq:dux_dt}
\frac{\partial u_x}{\partial t} = -u_x\frac{\partial u_x}{\partial x} -u_y\frac{\partial u_x}{\partial y} - \frac{1}{\rho}\frac{\partial P}{\partial x}.
\end{equation}
And similarly
\begin{equation}\label{eq:duy_dt}
\frac{\partial u_y}{\partial t} = -u_x\frac{\partial u_y}{\partial x} -u_y\frac{\partial u_y}{\partial y} - \frac{1}{\rho}\frac{\partial P}{\partial y} - g,
\end{equation}
where the last term is the aforementioned gravitational force divided by the density (acceleration in other word).

We now have equations of motion for our parameters.

\subsection{Ideal Gas Equations}\label{sec:ideal}
During our simulations we need t be able to describe more quantities than the four parameters. These quantities are pressure $P$ and temperature $T$. We have that pressure and energy relates as follows:
\begin{equation}\label{eq:P_to_e}
e = (\gamma - 1)P.
\end{equation}
And from the ideal gas law we know that
\begin{equation}\label{eq:ideal_gas}
P = \frac{\rho}{\mu m_u}k_B T,
\end{equation}
where $\mu = 0.61$, $m_u$ is the average unit atom mass and $k_B$ is the Boltzmann constant. Meaning that
\begin{equation}\label{eq:ideal_gas_with_e}
e = (\gamma - 1)\frac{\rho}{\mu m_u}k_B T.
\end{equation}
$\gamma$ is given as
\begin{equation}
\gamma = 1 + \frac{2}{f},
\end{equation}
where $f$ is the degrees of freedom of the gas. If we assume that the sun is mostly Hydrogen
(See corrections in sec. \ref{sec:corr}!), we have a diatomic gas with a degree of freedom of $5$, giving us 
\begin{equation}
\gamma = 1 + \frac{2}{5} = \frac{7}{5}\footnote{Given that we have a moderate part of Helium as well, this should more realistically be somewhere between 7/3 and 5/3.}.
\end{equation}
With these equations we should be able to simulate a simple fluid. \\

(Again: See corrections in sec. \ref{sec:corr}!)


\subsection{Calculating Convection Quantities}\label{sec:quantities}
We are interested in finding many of the parameters used in the previous project to describe convection. The first interesting quantity is the convection velocity. As in the lecture note we start by looking at a parcel having the buoyant force
\begin{equation}
f_B = -g\frac{\Delta \rho}{\rho}.
\end{equation}
Instead of the assumed distance the parcel travels is $\delta r$ we say that this is $dy$, and we can therefore use that $\Delta \rho = \frac{\partial \rho}{\partial y}\cdot dy$. Thus we get the work produced by the force as
\begin{equation}
\frac{1}{2}f_B\cdot dy = -\frac{g\cdot dy}{2\rho} \frac{\partial \rho}{\partial y}\cdot dy.
\end{equation}
Since this is equal to the kinetic energy, we get that the convection velocity is given as
\begin{equation}
v =\sqrt{-\frac{g\cdot dy\cdot dy}{\rho} \frac{\partial \rho}{\partial y}}.
\end{equation}
If the gravitation acts downward, then one of the $dy$'s will be negative, so as long as $\partial \rho/\partial y$ is positive, there should be no problem with complex velocities\footnote{or this is what I think/hope will happens...}. 

The next quantity is the convection energy flux $F_C$. Since we only have convection and no radiative energy transport, we can calculate $F_C$ numerically simply as 
\begin{equation}
F_{C,y} = \sum_{i = 0}^{Nx} e_{i,y}\cdot v_{i,y}\cdot dx,
\end{equation}
where the subscript $y$ indicates the depth where $F_C$ is calculated.
Next we want to find the temperature gradient $\nabla$. But first we will try to find the pressure scale hight defined as
\begin{equation}
H_P = -P\frac{\partial P}{\partial y}.
\end{equation}
This is a quantity with should be easy to calculate from our simulation. From this we can get the temperature gradient
\begin{equation}
\nabla = \frac{\partial \ln T}{\partial \ln P} = -\frac{H_P}{T}\frac{\partial T}{\partial y}.
\end{equation}
This is one of the crucial quantities we use to determine if we have convection or not. Convection is present if $\nabla > 2/5$. The temperature difference between the upgoing and downgoing flows $\Delta T$ should be easy to find from  the simulations by looking at the difference in temperature between the areas with positive vertical velocity and those with negative vertical velocity. Using the the definition of $\Delta T$ found in the lecture notes we should be able to find $(\nabla - \nabla_p)$ where $\nabla$ is the temperature gradient of the star, and $\nabla_p$ that of the parcel. This is found as
\begin{equation}
\Delta T = (\nabla - \nabla_p)\frac{T\cdot dy}{H_P} \Leftrightarrow (\nabla - \nabla_p) = \frac{\Delta T \cdot H_P}{T\cdot dy}.
\end{equation}
Lastly, having all of these quantities we can find the mixing length $l_m$ from an equation for the convective flux
\begin{equation}
F_C = \rho c_P v \frac{Tl_m}{2H_P}(\nabla - \nabla_p) \Leftrightarrow l_m = \frac{2F_C H_P}{(\nabla - \nabla_p)\rho T c_P v}.
\end{equation}
These are the quantities we are interested in looking at after having simulated convection. We can see that most of these differs in the different heights, so we have to look at this height dependence influence in the simulations.



\section{Numerical Theory}
\subsection{Schemes}\label{sec:schemes}
Which schemes we use to differentiate our variables numerically is an important part of any numerical project. For many fluid dynamical simulations the upwind scheme for differentiation is used. This is because this scheme ensures better stability for simulation of flow fields. We will not use this scheme, and instead trust our time integration to stabilize the simulation. We will instead use the central difference scheme, given as
\begin{equation}
\left[\frac{\partial \phi}{\partial x_i}\right]_{i}^n = \frac{\phi_{i+1}^n - \phi_{i-1}^n}{2 dx_i},
\end{equation}
where $\phi$ is the variable we want to differentiate, and $dx_i$ is the step length in the direction we are differentiating. This method is of order $O (dx_i)^2$ and should give the desired precision. There will be problems at the boundaries where $\phi_{i+1}^n$ or $\phi_{i-1}^n$ may not exist. For the horizontal boundaries we will have periodic boundaries, so the neighbours will always exist. For the vertical boundaries we need to be careful. We can use normal backward/forward Euler schemes, but we want more precision, so we are going to use a scheme found in a pdf handout given to us by Charalambos\footnote{\textit{Unknown Author,Selected Topics in Numerical Analysis, Chap. 4. Overview of the Finite Difference Calculus, eq. 4.18}}. This has the form
\begin{equation}\label{eq:better_diff}
\left[\frac{\partial \phi}{\partial x_i}\right]_{i}^n = \frac{-\phi^n_{i+2} + 4\phi^n_{i+1} - 3\phi^n_{i}}{2dx_i}.
\end{equation}
As with the central difference scheme, this is of order $O(dx_i)^2$, so will give us a good precision on the vertical boundaries.
\subsection{Restrictions on $dt$}
It is important to vary the time step $dt$ such that the integration of the variables does not over/undershoot to much. How we vary the $dt$ is done in much of the same way as in the previous projects, where the relative change of the variables are found as
\begin{equation}
\text{rel}(\phi) = \bigg|\frac{\partial \phi}{\partial t}\cdot \frac{1}{\phi} \bigg|,
\end{equation}
and the largest such relative change is called $\delta = \max \text{rel}(\phi)$. We the say that this change during a time step is no bigger than a $p=0.1$. The time steps is so set as
\begin{equation}
dt = \frac{p}{\delta}.
\end{equation}
New to this project is that we need to make sure that the velocities do not exceed the speed of sound, so that a particle can move more than one grid point during one time step. This takes more or less the same form as for the other variables, but
\begin{equation}
\text{rel}(x_i) = \bigg|\frac{\partial x_i}{\partial t}\cdot \frac{1}{dx_i} \bigg| = \bigg|u_i\cdot \frac{1}{dx_i} \bigg|.
\end{equation}
We then use this in the calculation of $\delta$ and $dt$ in the same way as with the other $\text{rel}(\phi)$.


\subsection{Stepping in Time}\label{sec:step}
When simulating fluids we need to be careful when we choose how to differentiate and integrate. As we saw in sec. \ref{sec:schemes} we are now using upwind differentiation, so normal Euler schemes for integrations can be volatile. We also had no viscosity (sec. \ref{sec:eq_motion}), so we have no diffusion, which is a problem. We are going to use a method of stepping in time which solves both our problems. We are going to step forward by
\begin{equation}
\phi_{i,j}^{n+1} = \left[\frac{\partial \phi}{\partial t}\right]_{i,j}^{n} + \overline{\phi}_{i,j}^{n},
\end{equation}
where $\phi$ is the variable we wish to integrate, and $\overline{\phi}_{i,j}^{n}$ is what makes this method interesting. This is the average of the neighbours of ${\phi}_{i,j}^{n}$. The problem here is which neighbours we take the average of. One possibility is just the neighbours above, below and on the sides, and another possibility is to take the average of all eight neighbours. Which of these options we choose will have some consequences on the simulation. In my experience choosing only the neighbours on the above, below and on the sides leads to strange diagonal waves which seems to come from the integration and not the physics. This is why we will use all eight neighbours. There will be problems on the boundaries. The horizontal boundaries are periodic, so they pose no problems. On the vertical boundaries we will only use the neighbours on the sides. 


\subsection{Initial Conditions}\label{sec:initial}
We are not looking to simulate some simple fluid, but the outer layer of the sun. This means that we need to construct the initial conditions we need for the sun. We have three important criteria. The first is that we start in hydrostatic equilibrium, this means that 
\begin{equation}\label{eq:hydrostatic}
\frac{\partial P}{\partial y} = -\rho g.
\end{equation}
This means that if we know the pressure at the top of the star, we can use this to work our way down layer by layer.
The second criterion is that the star need to be convectional unstable, meaning that
\begin{equation}
\nabla = \frac{\partial \ln T}{\partial \ln P} = \frac{P}{T}\frac{\partial T}{\partial P} > \frac{2}{5}.
\end{equation}
As with the pressure we want something that tells us how the temperature changes with height. Using that $\partial T /\partial P = \partial T /\partial y\cdot\partial y /\partial P$ and \eqref{eq:hydrostatic} we get that
\begin{equation}\label{eq:dT_dy}
\frac{\partial T}{\partial y} = -\nabla g \rho \frac{T}{P},
\end{equation}
where $\nabla$ is some number slightly larger than $2/5$ (and not the del operator). Using the values for $T$ and $P$ for the outer photosphere found in the lecture notes, and the above equations we can get our initial values as follows
\begin{enumerate}
\item Set values of $T$ and $P$ for the top of the photosphere of the sun
\item For each layer in the height of the grid:
\begin{itemize}
\item Calculate the next steps of temperature and pressure using \eqref{eq:hydrostatic} and \eqref{eq:dT_dy} and a simple Euler integration scheme.
\item Calculate energy and density from \eqref{eq:P_to_e} and \eqref{eq:ideal_gas}.
\end{itemize}
\end{enumerate}

This gives the initial conditions for the energy and density. The last criterion is that for the velocities, we simply set them to $0$ everywhere in the grid. Thus we have all the initial values for our four parameters.

\subsection{Boundaries}\label{sec:bound}
One important aspect of solving differential equations is the boundaries. It is important that our boundaries somewhat resembles what we expect to find in a star, and that they enables the existence of convection. For the horizontal boundaries we want something that emulates the fact that we are only looking on a small chunk of the star, and that the convection zone continues on both sides of that chunk. This is best done with periodic boundaries. In the code this is done with the \textit{numpy} function \textit{roll}.

The vertical boundaries are more tricky. For the velocities we simply have that
\begin{equation}
u_{y,B} = 0, \qquad \frac{\partial u_x}{\partial y}\bigg|_{y=B} = 0,
\end{equation}
where the subscript $B$ indicates the vertical boundaries. The density and energy is more connected, and needs to be set in such a way to enable convection. We also want the vertical boundaries to be in hydrostatic equilibrium \eqref{eq:hydrostatic}. Using \eqref{eq:P_to_e} we then get
\begin{equation}\label{eq:p_B}
\frac{\partial P}{\partial y} = \frac{1}{\gamma -1 }\frac{\partial e}{\partial y} = -\rho g \Leftrightarrow \rho = -\frac{1}{(\gamma - 1)g}\frac{\partial e}{\partial y}.
\end{equation}
We can insert this into \eqref{eq:ideal_gas} to get
\begin{equation}
e = -\frac{k_B T_B}{\mu m_u g}\frac{\partial e}{\partial y}.
\end{equation}
The value of $T_B$ is discussed below. If we use the differentiation scheme \eqref{eq:better_diff} we can get the boundary energy $e_B = e_i$
\begin{equation}\label{eq:bound_e}
e_i = \left[-\frac{2dy \mu m_u g }{T_B k_B}+3\right]^{-1}\left(4e_{i+1} - e_{i+2}\right).
\end{equation}
Since hydrostatic equilibrium is baked into this equation, we can use \eqref{eq:ideal_gas_with_e} to get the density of the boundaries. This should ensure hydrostatic equilibrium. $T_B$ is the temperatures at the boundaries. For the upper boundary (the top of the star) this is set to $0.9$ times the initial temperature, and at the lower boundary (the bottom of the star) this is set to $1.1$ times the initial temperature. This is to simulate the energy production of the lower parts of the star, and the radiative cooling of the upper part of the star, and thus the temperature gradient which enables convection.\\

There is another easier way to make boundary conditions for the energy. We can instead impose conditions on $\partial e/\partial t$ so that the boundary energy is pushed towards a high energy (and thus temperature) at the bottom and a low energy at the top. This is done with
\begin{equation}\label{eq:alt_de_dt_B}
\frac{\partial e}{\partial t} = -\frac{e -  e_B}{\delta},
\end{equation}
where $e_B$ is a boundary energy defined in the same way as $T_B$ and $\delta$ is a constant approximately $10\cdot dt$. To ensure hydrostatic equilibrium we need to set the boundary density using \eqref{eq:p_B}.

\section{Methods}

\subsection{The Structure of the Code}
The code i structured into 3 files/classes. \verb|convection2d.py| holds the main class holding all the tools necessary for doing the physics, \textit{Convection2D}. \verb|simulate.py| holds the class \textit{Simulate} that keeps track of the simulation and animation. Lastly \verb|analyzer.py| has functions for calculating the quantities found in sec. \ref{sec:quantities}. For the last file, the functions are quite self explanatory, but we will look at the flow in the other two files.

The main task of \verb|simulate.py| and \textit{Simulate} is to hold all the current density, velocity and energy, and to step them forward in time. This is done with the \textit{step} method. This method does (1) asks \textit{Convection2D} to update all its derivatives, (2) asks it to find the correct $dt$, and (3) asks for the next values of the parameters. This method will also calculate pressure and temperature from methods of \textit{Convection2D}. It will lastly return $dt$ as is required by \verb|Fvis|. It also has a method that saves the last values of the parameters to numpys file format \textit{.npy}, which are read and analysed by \verb|analyzer.py|.

\verb|convection2d.py| and \textit{Convection2D} holds the core methods of the project, and the ones called by \textit{Simulate}. The first important method is \textit{distribute\_initial\_conditions\_solar\_surface(P\_top,T\_top)} which, given the pressure and temperature of the upper photosphere, calculates the initial values. We can now use other methods to step forward in time. Since arrays holding the current differentiations both in spacial directions (sec. \eqref{sec:schemes}) and time (sec. \ref{sec:eq_motion}) are global, then this is the first method that has to be called when stepping in time. This will in turn call on the two functions \textit{update\_position\_derivatives(u\_x,u\_y,density,energy)} and \textit{update\_time\_derivatives(u\_x,u\_y,density,energy)} which will update all the derivatives through methods for each derivative. Now all other methods have access to the updated/current derivatives. The \textit{tune\_dt(u\_x,u\_y,density,energy,p=0.1)} method can then be called, which finds an appropriate $dt$. Lastly, a \textit{ get\_next\_} 
for each variable can be called, which will find the next step using what is described in sec. \ref{sec:step}. There are also some methods here for calculating pressure and temperature, using equations found in sec. \ref{sec:ideal}. Running \verb|convection2d.py| will run a test function to check if the spacial derivatives work, using a sine function.

This is the main flow of my files. In \verb|simulate.py|  \verb|Fvis| will be used to save and animate, before using its own method to save the last data point and delete the data made by \verb|Fvis|.

\subsection{Setting Up the Simulation}\label{sec:setup}
Here I will outline how my grid is build. Unlike what was done in the project description, I start the top of my star at index $0$ in the y-direction. I also was foolish enough to index my grids as $(x,y)$, instead of $(y,x)$ which is the way \textit{numpy} index matrices. Since I placed the top of the sun at the bottom of the array, this means that the simulations are done upside down. The main consequences will be that gravity works upward. This means that in all equations using gravity, we must use $-g \rightarrow g$ in the code, to get the gravity to work up. This will also mean that a positive velocity in the y-direction goes from the top to the bottom. To compensate for this, when animating and calculating quantities from sec. \ref{sec:quantities}, we will use $u_y \rightarrow -u_y$.\\

When running the simulation, we will use that the temperature at the top of the suns photosphere is $5772$K, and the pressure $1.8\cdot 10^8$Pa. For the instability criterion $\nabla$, we will use that this is $2/5 + 0.1$. All of the simulation will be run with the energy and density boundary conditions defined in \eqref{eq:bound_e} and \eqref{eq:ideal_gas_with_e}, since these were the only ones I got to work. 

\subsection{Running the Code}
The first thing we will do is to place two boxes with higher energy, one to the left and one to the right of the middle of the grid. This is to check if we get shock waves in the fluid. To get a clear shock wave, the energy in these boxes will be $10$ times the energy of their surroundings. This should show that what we are simulating behaves like a fluid. \\

Next we wish to see if we manage to get convection. We will need to look at some different boundary conditions for the energy and density, to see which will make convection (if any). Since we lack any initial velocity in the x-direction, we may need to force such velocities. There are two ways of doing this. One is to add small normal distributed noise to the x velocity. This should be enough to get a cascade going and a convection started. If this is not enough we may add the same two boxes as we did in the shock wave test. These boxes will also have $10$ times higher energy than their surroundings. This may be more effective in creating/sparking the circular motion we want to see in a convection cell.\\

Should we be able to get convection, we will use the equations from sec. \ref{sec:quantities} to look at some of the quantities we simulated in the previous project, and see if they corresponds with what we expect from convection.

\section{Results}
All the plots below has the top of the sun at the top, and the bottom at the bottom. A video of the shock wave is found as \verb|shockwave_video.mp4|, and a video of the ''convection'' is found as \verb|convection_video.mp4|.
\subsection{Shock waves}
\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.8\textwidth]{FVis_output_2018-05-25_15-17_snapshot_t550227}}\\
\subfloat[][]{\includegraphics[width=.8\textwidth]{FVis_output_2018-05-25_15-17_snapshot_t109363}}
\caption{Here two boxes with $10$ times the energy of the surroundings are placed inside the sun with the initial conditions given in sec. \ref{sec:initial}. (a) is taken after $5$ seconds and (b) after $10$ seconds. We can see that a shock wave pushes out from where the initial high energies was placed. From (b) we see that a more prominent wave travels downwards. I think this may have to do with the already higher energy lower down, therefore making the wave more prominent here, even though the amount of energy travelling up and down is the same. }\label{fig:shock}
\end{figure}

We see from fig. \ref{fig:shock} that we indeed get a shock wave in the fluid, and we therefore can take the implementation of the functions in sec. \ref{sec:eq_motion} to be correct. We can move on to convection.

\subsection{Convection}
\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.45\textwidth]
{FVis_output_2018-05-25_15-49_snapshot_t10}}
\subfloat[][]{\includegraphics[width=.45\textwidth]{FVis_output_2018-05-25_15-49_snapshot_t1990}}\\
\subfloat[][]{\includegraphics[width=.45\textwidth]{FVis_output_2018-05-25_15-47_snapshot_t10}}
\subfloat[][]{\includegraphics[width=.45\textwidth]{FVis_output_2018-05-25_15-47_snapshot_t1990}}
\caption{Here we can see simulations of pressure ((a) and (b)) and the temperature contrast ((c) and (d)). (a) and (c) are taken after $5$ seconds, and (b) and (d) after $1990$ seconds, roughly 33 minutes. We can see from the pressure plot that we at the start have the distribution of pressure that we want. We should after $33$ minutes have more or less the same distribution, but we see that the pressure have overall decreased. Moreover, the mass and energy deviation is about $-30\%$, meaning that mass and energy are not conserved, this is a not necessarily disastrous, due to imprecise differentiation schemes, but it is a sign that something might be wrong. The temperature contrast after $33$ minutes seems to show that the temperature at the top is increasing, but so is the temperature in the rest of the grid. This indicates that there might be more warming of the grid, than cooling. But that the main increase in temperature is at the top, is something we want to see in convection.}\label{fig:conv_no_explosion}
\end{figure}

We see from fig. \ref{fig:conv_no_explosion} that we do indeed get a high increase of temperature near the top, which we want in convection, but there are no convection cells present, so just letting the simulation run with the initial conditions is not enough. We also see that the pressure has decreased in the whole box, this is not a good sign. We need to try to provoke the convection cells to appear.

\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.8\textwidth]{FVis_output_2018-05-25_16-25_snapshot_t199529}}\\
\subfloat[][]{\includegraphics[width=.8\textwidth]{FVis_output_2018-05-25_16-27_snapshot_t199147}}
\caption{Here we can see the two methods of trying to invoke convection. In (a) we can see the temperature contrast of a simulation with $u_x$ initialized with a normal distribution with $\sigma = 100$ m/s. (b) shows the simulations with the same boxes of energy used in fig. \ref{fig:shock}. Both are taken after $33$ minutes. (a) is more or less the same as in fig. \ref{fig:conv_no_explosion}, but with some small chaotic waves. In (b) we have something which looks like convection!}\label{fig:conv_explosion}
\end{figure}

We can see from fig. \ref{fig:conv_explosion} (b) that by placing the same high energy boxes as in fig. \ref{fig:shock}, we get something that looks like convection cells. We see that the increased energy from the boxes have increased the temperature of the whole grid, but spots of hotter gas lie at the top of what looks like cells, with somewhat colder gas descending between the cells. I am somewhat sceptical if this is real convection, since the temperature is now higher in the grid than the temperature we enforce on the lower boundary. But, these cells seem to be stable, so they might be convection cells. We can see that the energy and mass deviation is somewhat worse now, but not by that much.

\subsection{Calculating the Convection Quantities}\label{sec:calc_quant}
We are to look at some of the quantities from sec. \ref{sec:quantities}. Since the nearest we got to convection was the simulation from fig. \ref{fig:conv_explosion} (b), this will be used. We start by looking at the convective flux. We see from fig. \ref{fig:fc} (a) that $F_C$ is negative, this is not what we expected due to convection carrying energy upwards. Had this been positive, then the shape would be what we are expecting, with the most flux being lower down, and as energy is used to warm the upper parts of the sun, the flux decreases.


\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.4\textwidth]{fc}}
\subfloat[][]{\includegraphics[width=.4\textwidth]{dt}}\\
\subfloat[][]{\includegraphics[width=.4\textwidth]{nabla_diff}}
\subfloat[][]{\includegraphics[width=.4\textwidth]{lm}}
\caption{Convective flux $F_c$, temperature difference $\Delta T$, $(\nabla^* -\nabla_p)$ and the mixing length $l_m$ of our simulated convection zone. $\Delta T$, $(\nabla^* -\nabla_p)$ and $l_m$ are calculated layer by layer by taking the average over that layer for all the variables used to calculate them.}\label{fig:fc}
\end{figure}

Calculating the convection velocity we get a range of $v \in [2.46,631]$. The problem comes when looking at the average of $u_y$ in the grid, where we get $\overline{u_y} = -379$ m/s, which means the velocity goes down towards the center of the sun. But this makes some sense looking at \ref{fig:conv_explosion} (b), since most of the temperature contrast seems to move downward. But as with $F_C$ this is not what we would expect. But there are some velocities that come close to the convective velocity. At height index $8$, right below the surface of the system, we get $v = 310.13$ m/s and $u_y = 310.18$ m/s. There are also some imaginary convection velocities near the boundaries due to the way we calculate $v$. If we look at $\nabla^*$, we can see that they have a range of $\nabla^* \in [0.05,2.91]$. So while some places have $\nabla^* \leq 2/5$, most, with $95\%$ of the grid, have $\nabla^* > 2/5$, so convection is expected. If we look at the temperature difference $\Delta T$ in fig. \ref{fig:fc} (b) we see that temperature is only transported towards the surface in the top $80\%$ of the sun, and the temperature transported upwards is much lower than that transported downwards, which is not what we expect from convection. $(\nabla^* -\nabla_p)$, \ref{fig:fc} (c),  fairs even worse. From the last project we expected this to be a positive number, but this is not the case for the lower parts of our simulation. The mixing length $l_m$ is also negative at the same places as $dT$ and $(\nabla^* -\nabla_p)$, and has values of $\pm 10^{12}$ for most heights. I could not easily find values to compare this with, but due to this being higher than the box we are simulating, I think this seems to be a bad estimation.


\section{Conclusion}
We can see from fig. \ref{fig:shock} that we managed to get shock waves, showing that the implementations of the equations of motion \ref{sec:eq_motion} was most likely correctly done. 

By letting the system run without any interference, we did not get convection cells (fig. \ref{fig:conv_no_explosion}). It seems that the upper part is warmed and the lower cooled, but it is difficult to see if this is due to convection or some bug in my code. The decrease in pressure points to a bug, so does the mass and energy deviation.

Trying to invoke convections did not work when using just random initial $u_x$ (fig. \ref{fig:conv_explosion} (a)), and the result was more or less the same result as in the above case.

When using two boxes with $10$ times the energy than the surroundings, we got something that could look like convection cells. I am, as mentioned, not sure if this is real convection, and as we saw in sec. \ref{sec:calc_quant}, the energy flux seemed to transport energy down, not up. But it may be my calculation of $F_c$ which is wrong. We saw that  $\nabla^* > 2/5 = 0.4$ (\ref{sec:calc_quant}) for most of the grid, so convections should be taking place.

I strongly suspect that my boundary conditions are at fault for much of the problems of this project. This is due to my large loss of energy and mass, and the strange results from sec. \ref{sec:calc_quant}. I suspect that some of my reasoning in sec. \ref{sec:bound} may have been wrong, but it can be my implementations as well. I know that our differentiation schemes are of a low order, and that we can get loss of energy and mass due to this, but I also know that the choice of boundary conditions influenced the loss heavily. I tried using the boundary conditions given by \eqref{eq:alt_de_dt_B}, but that failed, even though they were given by the Professor.

As for the quantities calculated in sec. \ref{sec:calc_quant}, there are three reasons why they may be wrong. First is that the simulation is wrong, and the data was faulty. Secondly, my equations from sec. \ref{sec:quantities} may simply be wrong. Lastly: I confused my self a lot(!) with the orientation of the grid, so my logic of letting $u_y \rightarrow -u_y$ may be wrong. I looked at the vertical velocities when checking the shock waves, and seeing that the shock wave travelling towards the surface had positive velocity, and the wave moving down had negative velocity. So this points towards my assertion about $u_y$ being correct. 

All in all I felt that this project was more an exercise in programming and debugging than actual learning the physics of convection. We used more or less all our time to get the most basic simulations to work, leaving very little time to actually look at the many goals of the project, which seemed to be a larger part of the project. I also felt that we got poor feedback on whether our reasonings were correct. So instead of getting a clear yes or no on whether we were on the right track (for example with boundary conditions), we ended up debugging many aspects of the code at the same time, never knowing which was at fault. I know this reflects real life, but as a learning experience this was prohibiting.  

\section{Correction}\label{sec:corr}
Right before the deadline, I found out that I assumed that the sun is made up of Hydrogen gas. The sun is of course mostly made up of plasma (free hydrogen atoms), this would imply that $f=3$, and gives $\gamma = 5/3$. Not only that! I managed to use $\gamma = 7/3$ in my code :( Since I used all my time commenting on the results with this $\gamma$, I have no time left to do new simulations and comment on these results... Running with the correct $\gamma$ makes the convection cells and pattern in fig. \ref{fig:conv_explosion} (b) completely disappear. The shock wave should be more or less the same. I will hand in the report knowing that I used the wrong $\gamma$, due to no time left before the deadline... Please don't judge me too hard on this :(

\section{Acknowledgement}
I must thank must of my fellow students for a busy two weeks programming and debugging. Mostly I want to thank Metin San, for many discussions and comparisons of code.

\end{document}

