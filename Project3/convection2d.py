# -*- coding: utf-8 -*-
"""
Created on Fri May 18 16:23:46 2018

@author: dulte
"""

import numpy as np
import matplotlib.pyplot as plt

class Convection2D:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.dx = (x_interval[1]-x_interval[0])/Nx
        self.dy = (y_interval[1]-y_interval[0])/Ny           
        
        self.mu = 0.61
        self.unit_mass = 1.661e-27
        self.gamma = 1.4
        self.G = 6.674e-11
        self.g = self.G*1.989e30/(695508e3)**2
        self.k_B = 1.381e-23
                                 
        self.T_top = 5772
        self.T_bottom = 5772
        
        self.dt = None
        
        self.unsability_criterion = 2/5. + 0.1
        
    def get_pressure(self,energy):
        return 1./(self.gamma-1)*energy
    
        
    def dV_dx(self,V):        
        return (np.roll(V,1,axis=0) - np.roll(V,-1,axis=0))/(2*self.dx)
    
    def dV_dy_center(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = (V[:,2:]-V[:,:-2])/(2*self.dy)
        return dVdy
    
    def dV_dy(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = -(V[:,2:]-V[:,:-2])/(2*self.dy)
        dVdy[:,0] = (-V[:,2] +4*V[:,1]-3*V[:,0])/(2*self.dy)
        dVdy[:,-1] = (-V[:,-3] +4*V[:,-2]-3*V[:,-1])/(2*self.dy)
        return dVdy
        
    def du_dy(self,u):
        dudy = np.zeros_like(u)
        dudy[:,1:-1] = -(u[:,2:]-u[:,:-2])/(2*self.dy)
        return dudy
    
    def get_midpoint(self,V):
        return 0.5*(np.roll(V,1,axis=0) + np.roll(V,-1,axis=0))
    
    def get_boundary_energy(self,energy):

        T = self.T_top*0.9
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,0] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,1] - energy[:,2])

        T = self.T_bottom*1.1
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,-1] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,-2] - energy[:,-3])

        
        return energy
    
    def get_boundary_density(self,density,energy):

        T = self.T_top*0.9
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,0] = temp_factor*energy[:,0]

        T = self.T_bottom*1.1
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,-1] = temp_factor*energy[:,-1]

        return density
    
    
    def de_dt(self,energy,momentum_x,momentum_y,density):
        u_x = momentum_x/density
        u_y = momentum_y/density
        P = self.get_pressure(energy)
        dux_dx = self.dV_dx(u_x)
        duy_dy = self.du_dy(u_y)
        
        de_dx = self.dV_dx(energy)
        de_dy = self.dV_dy(energy)
        
      
      
        
        de_dt = -(energy + P)*(dux_dx + duy_dy) - (u_x*de_dx + u_y*de_dy)

        return de_dt
    
    
    def drho_dt(self,momentum_x,momentum_y,density,energy):
#        u_x = momentum_x/density
        u_y = momentum_y/density
#        dux_dx = self.dV_dx(u_x)
        duy_dy = self.du_dy(u_y)
#        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
        
        dmx_dx = self.dV_dx(momentum_x)
        
        
        
        
        #drho_dt = -density*(dux_dx +duy_dy) - u_x*drho_dx - u_y*drho_dy
        drho_dt = -dmx_dx - density*duy_dy - u_y*drho_dy
        
        return drho_dt
    
    
    def dmomentumx_dt(self,momentum_x,momentum_y,density,energy):
        u_x = momentum_x/density
        u_y = momentum_y/density
        dux_dx = self.dV_dx(u_x)
        dux_dy = self.du_dy(u_x)        
        duy_dy = self.du_dy(u_y)
        
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
        
        de_dx = self.dV_dx(energy)
        
        
        dmx_dt = -2*density*u_x*dux_dx - u_x*u_x*drho_dx -\
                -density*(u_x*duy_dy - u_y*dux_dy) - u_x*u_y*drho_dy-\
                         1./(self.gamma-1)*de_dx
        
        return dmx_dt
    
    
    def dmomentumy_dt(self,momentum_x,momentum_y,density,energy):
        u_x = momentum_x/density
        u_y = momentum_y/density
        dux_dx = self.dV_dx(u_x)
        
        duy_dy = self.du_dy(u_y)
        duy_dx = self.dV_dx(u_y)
        duy_dx[:,0] = 0
        duy_dx[:,-1] = 0
        
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
        
        de_dy = self.dV_dy(energy)
        
        
        dmy_dt = -density*u_x*duy_dx - density*u_y*dux_dx - u_x*u_y*drho_dx -\
                -2*density*u_y*duy_dy - u_y*u_y*drho_dy -1./(self.gamma-1)*de_dy#+ density*self.g
        
        return dmy_dt
    
    
    def dux_dt(self,u_x,u_y,density,energy):
        dux_dx = self.dV_dx(u_x)
        dux_dy = self.du_dy(u_x) 
        
        
        de_dx = self.dV_dx(energy)
        
        dux_dt = -u_x*dux_dx - u_y*dux_dy - 1./density*1./(self.gamma-1)*de_dx
        
        return dux_dt 

    def duy_dt(self,u_x,u_y,density,energy):
        duy_dy = self.du_dy(u_y)
        duy_dx = self.dV_dx(u_y)
        duy_dx[:,0] = 0
        duy_dx[:,-1] = 0
        
        de_dy = self.dV_dy(energy)
        
        dux_dt = -u_x*duy_dx - u_y*duy_dy - 1./density*1./(self.gamma-1)*de_dy
        
        return dux_dt                                                  
        
        

    def get_next_energy(self,energy,momentum_x,momentum_y,density):
        prev_energy_midpoint = self.get_midpoint(energy)
        de_dt = self.de_dt(energy,momentum_x,momentum_y,density)
        next_energy = prev_energy_midpoint + de_dt*self.dt
        next_energy = self.get_boundary_energy(energy)
        
        return next_energy
        
    
        
    def get_next_density(self,momentum_x,momentum_y,density,energy):
        drho_dt = self.drho_dt(momentum_x,momentum_y,density,energy)
        next_density = drho_dt*self.dt + self.get_midpoint(density)
        next_density = self.get_boundary_density(density,energy)
        
        return next_density
    
    def get_next_momentum_x(self,momentum_x,momentum_y,density,energy):
        prev_momentumx_midpoint = self.get_midpoint(momentum_x)
        dmx_dt = self.dmomentumx_dt(momentum_x,momentum_y,density,energy)
        next_momentum_x =dmx_dt*self.dt + prev_momentumx_midpoint
        
        return next_momentum_x
    
    def get_next_momentum_y(self,momentum_x,momentum_y,density,energy):
        prev_momentumy_midpoint = self.get_midpoint(momentum_x)
        dmy_dt = self.dmomentumy_dt(momentum_x,momentum_y,density,energy)
        next_momentum_y =dmy_dt*self.dt + prev_momentumy_midpoint
        next_momentum_y[:,0] = 0
        next_momentum_y[:,-1] = 0
        
        return next_momentum_y
    
    def get_next_u_x(self,u_x,u_y,density,energy):
        pass
    
    def tune_dt(self,momentum_x,momentum_y,density,energy,p=0.1):
        u_x = momentum_x/density
        u_y = momentum_y/density
        de_dt = self.de_dt(energy,momentum_x,momentum_y,density)
        drho_dt = self.drho_dt(momentum_x,momentum_y,density,energy)
        dmx_dt = self.dmomentumx_dt(momentum_x,momentum_y,density,energy)
        dmy_dt = self.dmomentumy_dt(momentum_x,momentum_y,density,energy)
        
        dV_dt = np.array([de_dt,drho_dt,dmx_dt,dmy_dt])
        V = np.array([energy,density,momentum_x,momentum_y])
        
        delta = np.nanmax(np.abs(dV_dt/V))
        
        self.dt = p/delta
        if self.dt < 0.001:
            self.dt = 0.001
        
#        if np.max(np.abs(u_x)) != 0 and np.max(np.abs(u_y)) != 0:
#            speed_limit_x = p*self.dx/(np.max(np.abs(u_x)))
#            speed_limit_y = p*self.dy/(np.max(np.abs(u_y)))
#            
#            if self.dt > min([speed_limit_x,speed_limit_y]):
#                self.dt = min([speed_limit_x,speed_limit_y])
            
    
    def distribute_initial_conditions_solar_surface(self,P_top,T_top):
        self.T_top = T_top
        density = np.zeros((self.Nx,self.Ny))
        energy = np.zeros_like(density)
        momentum_x = np.zeros_like(density)
        momentum_y = np.zeros_like(density)
        pressure = np.zeros_like(density)
        temperature = np.zeros_like(density)
        
        temp_factor = self.mu*self.unit_mass/self.k_B
        
        pressure[:,0] = P_top
        temperature[:,0] = T_top
        energy[:,0] = (self.gamma - 1)*pressure[:,0]
        density[:,0] = temp_factor*pressure[:,0]/temperature[:,0]
        
        for i in range(1,self.Ny):
           pressure[:,i] = pressure[:,i-1] + self.g*self.dy*density[:,i-1]
           temperature[:,i] = temperature[:,i-1] + self.unsability_criterion*self.dy*self.g*\
                                        density[:,i-1]*temperature[:,i-1]/pressure[:,i-1]
                        
           energy[:,i] = (self.gamma - 1)*pressure[:,i]
           density[:,i] = temp_factor*pressure[:,i]/temperature[:,i]  
           
           
        self.T_bottom = temperature[-1,-1]
        return momentum_x,momentum_y,density,energy
    
    
    def distribute_initial_conditions_sanity(self,density,energy):
        density = np.ones((self.Nx,self.Ny))*density
        density[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] /= 1000 
                        
        energy = np.ones_like(density)*energy
        energy[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] /=10
        momentum_x = np.zeros_like(density) + 50
        momentum_y = np.zeros_like(density) 
        
        temp_factor = self.mu*self.unit_mass/self.k_B
        
        self.T_top = energy[0,0]/(self.gamma-1)*temp_factor/density[0,0]/0.9
        self.T_bottom = energy[0,0]/(self.gamma-1)*temp_factor/density[0,-1]/1.1
        
        return momentum_x,momentum_y,density,energy
        
        