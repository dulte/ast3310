# -*- coding: utf-8 -*-
"""
Created on Fri May 18 16:23:46 2018

@author: dulte
"""

import numpy as np
import matplotlib.pyplot as plt

class Convection2D:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.dx = (x_interval[1]-x_interval[0])/Nx
        self.dy = (y_interval[1]-y_interval[0])/Ny

        self.mu = 0.61
        self.unit_mass = 1.661e-27
        self.gamma = 5./3#1.4
        self.G = 6.674e-11
        self.g = self.G*1.989e30/(695508e3)**2
        self.k_B = 1.381e-23

        self.T_top = 5772
        self.T_bottom = 5772

        self.dt = None

        self.unsability_criterion = 2/5. + 0.1

    def get_pressure(self,energy):
        return 1./(self.gamma-1)*energy

    def get_energy_from_temperature(self,T,density):
        return (self.gamma-1)*density/(self.mu*self.unit_mass)*self.k_B*T

    def dV_dx(self,V):
        return (np.roll(V,-1,axis=0) - np.roll(V,1,axis=0))/(2*self.dx)

    def dV_dy_center(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = (V[:,2:]-V[:,:-2])/(2*self.dy)
        return dVdy

    def dV_dy(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = ((np.roll(V,-1,axis=1) - np.roll(V,1,axis=1))/(2*self.dx))[:,1:-1]#(V[:,2:]-V[:,:-2])/(2*self.dy)
        dVdy[:,0] = (-V[:,2] +4*V[:,1]-3*V[:,0])/(2*self.dy)
        dVdy[:,-1] = -(-V[:,-3] +4*V[:,-2]-3*V[:,-1])/(2*self.dy)
        return dVdy

    def du_dy(self,u,uy):
        dudy = np.zeros_like(u)
        dudy[:,1:-1] = ((np.roll(u,-1,axis=1) - np.roll(u,1,axis=1))/(2*self.dx))[:,1:-1]#(u[:,2:]-u[:,:-2])/(2*self.dy)
        return dudy

    def get_midpoint(self,V):
        
        midpoint = 0.25*(np.roll(V,1,axis=0) + np.roll(V,-1,axis=0) + np.roll(V,1,axis=1) + np.roll(V,-1,axis=1))
        midpoint[:,0] = V[:,0]
        midpoint[:,-1] = V[:,-1]
        return midpoint

    def get_boundary_energy(self,energy):

        T = self.T_top*0.9
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,0] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,1] - energy[:,2])

        T = self.T_bottom*1.1
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,-1] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,-2] - energy[:,-3])


        return energy

    def get_boundary_density(self,density,energy):

        T = self.T_top*0.9
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,0] = temp_factor*energy[:,0]

        T = self.T_bottom*1.1
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,-1] = temp_factor*energy[:,-1]

        return density

    def accelerate_convection(self,de_dt,energy,density):
        delta = 0.1
        de_dt[:,1] = (energy[:,2]- self.get_energy_from_temperature(self.T_top,density[0,2])*1.1)/delta
        de_dt[:,-2] = (energy[:,-2]- self.get_energy_from_temperature(self.T_top,density[0,-2])*0.9)/delta

        return de_dt

    def de_dt(self,u_x,u_y,density,energy):
        P = self.get_pressure(energy)
        dux_dx = self.dV_dx(u_x)
        duy_dy = self.du_dy(u_y,u_y)

        de_dx = self.dV_dx(energy)
        de_dy = self.dV_dy(energy)

        de_dt = -(energy + P)*(dux_dx + duy_dy) - (u_x*de_dx + u_y*de_dy) + self.g
        de_dt = self.accelerate_convection(de_dt,energy,density)
        return de_dt


    def drho_dt(self,u_x,u_y,density,energy):
        return -self.dV_dx(density*u_x) -self.dV_dy(density*u_y)
        dux_dx = self.dV_dx(u_x)
        duy_dy = self.dV_dy(u_y)#self.du_dy(u_y,u_y)
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)



        drho_dt = -density*(dux_dx +duy_dy) - u_x*drho_dx - u_y*drho_dy
        #drho_dt = -dmx_dx - density*duy_dy - u_y*drho_dy

        return drho_dt



    def dux_dt(self,u_x,u_y,density,energy):
        dux_dx = self.dV_dx(u_x)
        dux_dy = self.du_dy(u_x,u_y)
        de_dx = self.dV_dx(energy)

        dux_dt = -u_x*dux_dx - u_y*dux_dy - 1./density*1./(self.gamma-1)*de_dx

        return dux_dt

    def duy_dt(self,u_x,u_y,density,energy):
        duy_dy = self.du_dy(u_y,u_y)
        duy_dx = self.dV_dx(u_y)
        duy_dx[:,0] = 0
        duy_dx[:,-1] = 0

        de_dy = self.dV_dy(energy)

        dux_dt = -u_x*duy_dx - u_y*duy_dy - 1./density*1./(self.gamma-1)*de_dy
        return dux_dt



    def get_next_energy(self,u_x,u_y,density,energy):
        prev_energy_midpoint = self.get_midpoint(energy)
        de_dt = self.de_dt(u_x,u_y,density,energy)
        next_energy = prev_energy_midpoint + de_dt*self.dt
        next_energy = self.get_boundary_energy(energy)

        return next_energy



    def get_next_density(self,u_x,u_y,density,energy):
        drho_dt = self.drho_dt(u_x,u_y,density,energy)
        next_density = drho_dt*self.dt + self.get_midpoint(density)
        next_density = self.get_boundary_density(density,energy)

        return next_density


    def get_next_u_x(self,u_x,u_y,density,energy):
        prev_ux = self.get_midpoint(u_x)
        dux_dt = self.dux_dt(u_x,u_y,density,energy)

        return prev_ux + dux_dt*self.dt

    def get_next_u_y(self,u_x,u_y,density,energy):
        prev_uy = self.get_midpoint(u_y)
        duy_dt = self.duy_dt(u_x,u_y,density,energy)
        next_uy = prev_uy + duy_dt*self.dt
        next_uy[:,0] = 0
        next_uy[:,-1] = 0

        return next_uy

    def tune_dt(self,u_x,u_y,density,energy,p=0.1):
        de_dt = self.de_dt(u_x,u_y,density,energy)
        drho_dt = self.drho_dt(u_x,u_y,density,energy)
        dux_dt = self.dux_dt(u_x,u_y,density,energy)
        duy_dt = self.duy_dt(u_x,u_y,density,energy)


        delta = [np.nanmax(np.abs(de_dt/energy)),np.nanmax(np.abs(drho_dt/density)),np.nanmax(np.abs(u_x/self.dx)),np.nanmax(np.abs(u_y/self.dy))]
        delta = np.nanmax(np.array(delta))
        # dV_dt = np.array([de_dt,drho_dt])
        # V = np.array([energy,density])
        #
        # delta = np.nanmax(np.abs(dV_dt/V))

        if delta != 0:
            self.dt = p/delta
        else:
            self.dt = 0.1


        if self.dt>100:
            self.dt = 100
        elif self.dt < 0.01:
            self.dt = 0.01
        #self.dt = np.nanmin(p*np.abs(V/dV_dt))


        # if np.max(np.abs(u_x)) != 0 and np.max(np.abs(u_y)) != 0:
        #     speed_limit_x = p*self.dx/(np.max(np.abs(u_x)))
        #     speed_limit_y = p*self.dy/(np.max(np.abs(u_y)))
        #
        #     if self.dt > min([speed_limit_x,speed_limit_y]):
        #         self.dt = min([speed_limit_x,speed_limit_y])


#        if self.dt < 0.00001 or self.dt == np.NAN:
#            self.dt = 0.00001
#
#
#        self.dt = 0.01


    def distribute_initial_conditions_solar_surface(self,P_top,T_top):
        self.T_top = T_top
        density = np.zeros((self.Nx,self.Ny))
        energy = np.zeros_like(density)
        u_x = np.zeros_like(density)
        u_y = np.zeros_like(density)
        pressure = np.zeros_like(density)
        temperature = np.zeros_like(density)

        temp_factor = self.mu*self.unit_mass/self.k_B

        pressure[:,0] = P_top
        temperature[:,0] = T_top
        energy[:,0] = (self.gamma - 1)*pressure[:,0]
        density[:,0] = temp_factor*pressure[:,0]/temperature[:,0]

        for i in range(1,self.Ny):
           pressure[:,i] = pressure[:,i-1] + self.g*self.dy*density[:,i-1]
           temperature[:,i] = temperature[:,i-1] + self.unsability_criterion*self.dy*self.g*\
                                        density[:,i-1]*temperature[:,i-1]/pressure[:,i-1]

           energy[:,i] = (self.gamma - 1)*pressure[:,i]
           density[:,i] = temp_factor*pressure[:,i]/temperature[:,i]

        #energy[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] *=1.5
        #density[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] *= 1.1
        self.T_bottom = temperature[-1,-1]
        return u_x,u_y,density,energy


    def distribute_initial_conditions_sanity(self,density,energy):
        density = np.ones((self.Nx,self.Ny))*density
        density[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] *= 10

        energy = np.ones_like(density)*energy
        energy[int(self.Nx/4.):int(3*self.Nx/4.),int(self.Ny/4.):int(self.Ny*3/4.)] *=10
        momentum_x = np.zeros_like(density)
        momentum_y = np.zeros_like(density)

        temp_factor = self.mu*self.unit_mass/self.k_B

        self.T_top = energy[0,0]/(self.gamma-1)*temp_factor/density[0,0]/0.9
        self.T_bottom = energy[0,0]/(self.gamma-1)*temp_factor/density[0,-1]/1.1
        print("Initial Temp: ",self.T_bottom,self.T_top)
        return momentum_x,momentum_y,density,energy


    #TWO DIMENTIONAL UPWIND X DIRECTION
    def upwind_x(self,f,ux):

        dx = self.dx
        nx = self.Nx
        nz = self.Ny
        upwind = np.zeros((nx,nz))
        #Axis = 1 is to ensure we roll in the x direction, otherwise works the same as 1D
        ddx_pos = (f - np.roll(f, 1,axis=0))/dx
        ddx_neg = (np.roll(f,-1,axis=0) - f)/dx

        upwind[np.where(ux>=0)] = ddx_pos[np.where(ux>=0)]
        upwind[np.where(ux<0)] = ddx_neg[np.where(ux<0)]

        return upwind

    #TWO DIMENTIONAL UPWIND Z DIRECTION
    def upwind_z(self,f,uz):

        dz = self.dy
        nx = self.Nx
        nz = self.Ny
        upwind = np.zeros((nx,nz))

        ddz_pos = (f - np.roll(f, 1,axis=1))/dz
        ddz_neg = (np.roll(f,-1,axis=1) - f)/dz

        upwind[np.where(uz>=0)] = ddz_pos[np.where(uz>=0)]
        upwind[np.where(uz<0)] = ddz_neg[np.where(uz<0)]

        return upwind

    def test_differentiation(self):
        grid = np.zeros((100,100))
        for i in range(100):
            grid[:,i] = np.sin(np.linspace(0,4*np.pi,100))

        diff_grid = self.dV_dx(grid)
        plt.plot(np.linspace(0,4*np.pi,100),grid[:,0],label="Sin")
        plt.plot(np.linspace(0,4*np.pi,100),diff_grid[:,0],label="Cos")
        plt.plot(np.linspace(0,4*np.pi,100),np.cos(np.linspace(0,4*np.pi,100)),label="Real Cos")
        plt.legend()
        plt.show()

        grid[:,:] = np.zeros((100,100))
        for i in range(100):
            grid[i,:] = np.sin(np.linspace(0,4*np.pi,100))

        diff_grid = self.dV_dy(grid)
        plt.plot(np.linspace(0,4*np.pi,100),grid[0,:],label="Sin")
        plt.plot(np.linspace(0,4*np.pi,100),diff_grid[0,:],label="Cos")
        plt.plot(np.linspace(0,4*np.pi,100),np.cos(np.linspace(0,4*np.pi,100)),label="Real Cos")
        plt.legend()
        plt.show()

if __name__=="__main__":
    con = Convection2D(100,100,(0,4*np.pi),(0,4*np.pi))
    con.test_differentiation()
