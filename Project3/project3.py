import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import ArtistAnimation
import seaborn as sns

    
class Grid:
    def __init__(self,x_steps,y_steps,x_interval,y_interval):
        self.dx = (x_interval[1]-x_interval[0])/x_steps
        self.dy = (y_interval[1]-y_interval[0])/y_steps                
        self.x_steps = x_steps
        self.y_steps = y_steps
                   
        self.grid_depth = 0
        self.grid = []
        self.time_differentials = []
        
        self.variable_register = {}
        
        self.time = []
        self.time_index = 0
        
        
        self.time_series = []
        
        self.dt = None
        
        
    def set_dt(self,p=0.1):
        grid = np.array(self.time_series[-1])
        rel_V = np.zeros(self.grid_depth)
        
        for i in range(self.grid_depth):
            rel_V[i] = np.max(np.abs(self.time_differentials[i](grid[:,:,:],self.time[-1]) \
                         /grid[i,:,:]))
        
        largest_rel_V = np.max(rel_V)
        self.dt = p/largest_rel_V

    def add_varable_to_grid(self,dVdt,var_name):  
        self.variable_register[var_name] = self.grid_depth
        self.time_differentials.append(dVdt)
        self.grid.append(np.zeros((self.x_steps,self.y_steps)))
        self.grid_depth += 1
    
    def get_index_of_varable(self,var_name):
        try:
            return self.variable_register[var_name]
        except:
            print("Unknown varable name")
            exit(1)
        
    def initiate_variable(self,function,var_name):
        index = self.get_index_of_varable(var_name)
        self.grid[index] = function(self.grid[index])
        
    
    def start_time_series(self):
        self.time.append(0)
        self.time_series.append(self.grid)
        
        
    def take_step(self,p=0.1):
        self.set_dt(p)
        
        grid = self.time_series[-1]
        next_grid = np.zeros_like(np.array(grid))
        for i in range(self.grid_depth):
            next_grid[i,:,:] = self.time_differentials[i](grid[:,:,:],self.time[-1])*self.dt + \
                     0.5*(np.roll(grid[i,:,:],1,axis=0) + np.roll(grid[i,:,:],-1,axis=0))
        
        
        self.time_series.append(next_grid)
        self.time.append(self.time[-1] + self.dt)
        self.time_index += 1
        
    
    def get_dVdx(self,var_name):
        index = self.get_index_of_varable(var_name)
        grid = np.array((self.time_series[-1])[index,:,:])
        return (np.roll(grid[:,:],1,axis=0) - np.roll(grid[:,:],-1,axis=0))/(2*self.dx)
    
    def get_dVdy(self,var_name):
        index = self.get_index_of_varable(var_name)
        grid = np.array((self.time_series[-1])[index,:,:])
        return (np.roll(grid[:,:],1,axis=1) - np.roll(grid[:,:],-1,axis=1))/(2*self.dy)
    
    def do_dVdx(self,grid):
        return (np.roll(grid[:,:],1,axis=0) - np.roll(grid[:,:],-1,axis=0))/(2*self.dx)
    
    def do_dVdy(self,grid):
        return (np.roll(grid[:,:],1,axis=1) - np.roll(grid[:,:],-1,axis=1))/(2*self.dy)




class Simulate:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.grid = Grid(Nx,Ny,x_interval,y_interval)
        
    
    def dedt(self,grid,t):
        e_index = self.grid.get_index_of_varable["Energy"]
        ux_index = self.grid.get_index_of_varable["ux"]
        uy_index = self.grid.get_index_of_varable["uy"]
        P_index = self.grid.get_index_of_varable["Pressure"]
        
        
        
        dedt = -self.grid.do_dVdx(grid[e_index,:,:]*grid[ux_index,:,:])-\
                self.grid.do_dVdy(grid[e_index,:,:]*grid[uy_index,:,:])-\
                grid[P_index,:,:]*(self.grid.do_dVdx(grid[ux_index,:,:])+\
                    self.grid.do_dVdy(grid[uy_index,:,:]))
                
        return dedt
    
    def drhodt(self,grid,t):
        rho_index = self.grid.get_index_of_varable["Density"]
        ux_index = self.grid.get_index_of_varable["ux"]
        uy_index = self.grid.get_index_of_varable["uy"]
        
        drhodt = -self.grid.do_dVdx(grid[rho_index,:,:]*grid[ux_index,:,:])-\
                    self.grid.do_dVdy(grid[rho_index,:,:]*grid[uy_index,:,:])
                    
        return drhodt
    
    
class Convection2D:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.grid = Grid(Nx,Ny,x_interval,y_interval)
        self.dx = (x_interval[1]-x_interval[0])/Nx
        self.dy = (y_interval[1]-y_interval[0])/Ny
                   
        self.density = [np.zeros((Nx,Ny))]
        self.momentum_x = [np.zeros((Nx,Ny))]
        self.momentum_y = [np.zeros((Nx,Ny))]
        self.energy = [np.zeros((Nx,Ny))]
        
        self.mu = 0.61
        self.unit_mass = 1.661e-27
        self.gamma = 1.4
        self.G = 6.674e-11
        self.g = self.G*1.989e30/(695508e3)**2
        self.k_B = 1.381e-23
                                 
        self.T_top = 5772
        self.T_bottom = 5772
        
        self.dt = None
        
    def get_pressure(self,energy):
        return 1./(self.gamma-1)*energy
        
    def dV_dx(self,V):
        return (np.roll(V,1,axis=0) - np.roll(V,-1,axis=0))/(2*self.dx)
    
    def dV_dy_center(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = (V[:,2:]-V[:,:-2])/(2*self.dy)
        return dVdy
    
    def dV_dy(self,V):
        dVdy = np.zeros_like(V)
        dVdy[:,1:-1] = (V[:,2:]-V[:,:-2])/(2*self.dy)
        dVdy[:,0] = (-V[:,2] +4*V[:,1]-3*V[:,0])/(2*self.dy)
        dVdy[:,-1] = (-V[:,-3] +4*V[:,-2]-3*V[:,-1])/(2*self.dy)
        return dVdy
        
    def du_dy(self,u):
        dudy = np.zeros_like(u)
        dudy[:,1:-1] = (u[:,2:]-u[:,:-2])/(2*self.dy)
        return dudy
    
    def get_midpoint(self,V):
        return 0.5*(np.roll(V,1,axis=0) + np.roll(V,-1,axis=0))
    
    def get_boundary_energy(self,energy):

        T = self.T_top*0.9
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,0] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,1] - energy[:,2])

        T = self.T_bottom*1.1
        temp_factor = self.k_B*T/(self.mu*self.unit_mass*self.g)
        energy[:,-1] =1./(2*self.dy/temp_factor + 3)*(4*energy[:,-2] - energy[:,-3])

        
        return energy
    
    def get_boundary_density(self,density,energy):

        T = self.T_top*0.9
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,0] = temp_factor*energy[:,0]

        T = self.T_bottom*1.1
        temp_factor = self.mu*self.unit_mass/((self.gamma - 1)*self.k_B*T)
        density[:,-1] = temp_factor*energy[:,-1]

        return density
    
    
    def de_dt(self,energy,momentum_x,momentum_y,density):
        u_x = momentum_x/density
        u_y = momentum_y/density
        P = self.get_pressure(energy)
        dux_dx = self.dV_dx(u_x)
        duy_dy = self.du_dy(u_y)
        
        de_dx = self.dV_dx(energy)
        de_dy = self.dV_dy(energy)
        
        de_dt = -(energy + P)*(dux_dx + duy_dy) - (u_x*de_dx + u_y*de_dy)
        
        return de_dt
    
    
    def drho_dt(self,momentum_x,momentum_y,density,energy):
        u_x = momentum_x/density
        u_y = momentum_y/density
        dux_dx = self.dV_dx(u_x)
        duy_dy = self.du_dy(u_y)
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
        
        drho_dt = -density*(dux_dx +duy_dy) - u_x*drho_dx - u_y*drho_dy
                           
        return drho_dt
    
    
    def dmomentumx_dt(self,momentum_x,momentum_y,density,energy):
        u_x = momentum_x/density
        u_y = momentum_y/density
        dux_dx = self.dV_dx(u_x)
        dux_dy = self.du_dy(u_x)        
        duy_dy = self.du_dy(u_y)
        
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
        
        de_dx = self.dV_dx(energy)
        
        
        dmx_dt = -2*density*u_y*dux_dx - u_x*u_x*drho_dx -\
                -density*(u_x*duy_dy - u_y*dux_dy) - u_x*u_y*drho_dy-\
                         1./(self.gamma-1)*de_dx
        
        return dmx_dt
    
    
    def dmomentumy_dt(self,momentum_x,momentum_y,density,energy):
        u_x = momentum_x/density
        u_y = momentum_y/density
        dux_dx = self.dV_dx(u_x)
        
        duy_dy = self.du_dy(u_y)
        duy_dx = self.dV_dx(u_y)
        duy_dx[:,0] = 0
        duy_dx[:,-1] = 0
        
        drho_dx = self.dV_dx(density)
        drho_dy = self.dV_dy(density)
             
        
        dmy_dt = -density*u_x*duy_dx - density*u_y*dux_dx - u_x*u_y*drho_dx -\
                -2*density*u_y*duy_dy - u_y*u_y*drho_dy - density*self.g
        
        return dmy_dt
        
        

    def get_next_energy(self,energy,momentum_x,momentum_y,density):
        prev_energy_midpoint = self.get_midpoint(energy)
        de_dt = self.de_dt(energy,momentum_x,momentum_y,density)
        next_energy = prev_energy_midpoint + de_dt*self.dt
        next_energy = self.get_boundary_energy(energy)
        
        return next_energy
        
    
        
    def get_next_density(self,momentum_x,momentum_y,density,energy):
        drho_dt = self.drho_dt(momentum_x,momentum_y,density,energy)
        next_density = drho_dt*self.dt + self.get_midpoint(density)
        next_density = self.get_boundary_density(density,energy)
        
        return next_density
    
    def get_next_momentum_x(self,momentum_x,momentum_y,density,energy):
        prev_momentumx_midpoint = self.get_midpoint(momentum_x)
        dmx_dt = self.dmomentumx_dt(momentum_x,momentum_y,density,energy)
        next_momentum_x =dmx_dt*self.dt + prev_momentumx_midpoint
        
        return next_momentum_x
    
    def get_next_momentum_y(self,momentum_x,momentum_y,density,energy):
        prev_momentumy_midpoint = self.get_midpoint(momentum_x)
        dmy_dt = self.dmomentumy_dt(momentum_x,momentum_y,density,energy)
        next_momentum_y =dmy_dt*self.dt + prev_momentumy_midpoint
        next_momentum_y[:,0] = 0
        next_momentum_y[:,-1] = 0
        
        return next_momentum_y
    
    def tune_dt(self,momentum_x,momentum_y,density,energy,p=0.1):
        u_x = momentum_x/density
        u_y = momentum_y/density
        de_dt = self.de_dt(energy,momentum_x,momentum_y,density)
        drho_dt = self.drho_dt(momentum_x,momentum_y,density,energy)
        dmx_dt = self.dmomentumx_dt(momentum_x,momentum_y,density,energy)
        dmy_dt = self.dmomentumy_dt(momentum_x,momentum_y,density,energy)
        
        dV_dt = np.array([de_dt,drho_dt,dmx_dt,dmy_dt])
        V = np.array([energy,density,momentum_x,momentum_y])
        
        delta = np.max(dV_dt/V)
        
        self.dt = p/delta
        
        speed_limit_x = p*self.dx/(np.max(np.abs(u_x)))
        speed_limit_y = p*self.dy/(np.max(np.abs(u_y)))
        
        if self.dt > min([speed_limit_x,speed_limit_y]):
            self.dt = min([speed_limit_x,speed_limit_y])
            
        


    
    
    

if __name__ == '__main__':
    pass
