# -*- coding: utf-8 -*-
"""
Created on Fri May 18 16:24:47 2018

@author: dulte
"""

import numpy as np
import matplotlib.pyplot as plt
import FVis3 as FVis


from convection2d import Convection2D


class Simulate:
    def __init__(self,Nx,Ny,x_interval,y_interval):
        self.Nx = Nx
        self.Ny = Ny
        self.dx = (x_interval[1]-x_interval[0])/Nx
        self.dy = (y_interval[1]-y_interval[0])/Ny
                  
                

        self.convection = Convection2D(Nx,Ny,x_interval,y_interval)

        self.T_top = 5772 #K
        self.P_top = 1.8e8#Pa

        
        self.u_x,self.u_y, self.density, self.energy = self.convection.distribute_initial_conditions_solar_surface(self.P_top,self.T_top)

        

        self.pressure = np.zeros_like(self.u_x)
        self.temperature = np.zeros_like(self.u_x)
        
        
        """
        Since use (x,y) instead of (y,x) as in numpy arrays, the arrays
        have to be transposed. And since I have the top of the sun at j=0,
        the arrays have to be flipped so to have the top at the top. This is 
        a cumbersome way to do this, I know...
        """
        
        self.correct_energy = np.zeros_like(self.u_x.T)
        self.correct_density = np.zeros_like(self.u_x.T)
        self.correct_u_x = np.zeros_like(self.u_x.T)
        self.correct_u_y = np.zeros_like(self.u_x.T)
        
        self.correct_temperature = np.zeros_like(self.u_x.T)
        self.correct_pressure = np.zeros_like(self.u_x.T)




    def step(self):
        """
        A step function as spesified by the Fvis documentation.
        This will update the derivatives, then tune dt, step one
        step forward in time, and finally transpose and flip the arrays
        to get the correct orientation.
        """
        self.convection.update_all_derivatives(self.u_x,self.u_y,self.density,self.energy)
        self.convection.tune_dt(self.u_x,self.u_y,self.density,self.energy)
        next_energy = np.zeros_like(self.energy)
        next_density = np.zeros_like(self.energy)
        next_u_x = np.zeros_like(self.energy)
        next_u_y = np.zeros_like(self.energy)

        next_energy[:,:] = self.convection.get_next_energy(self.u_x,self.u_y,self.density,self.energy)
        next_density[:,:] = self.convection.get_next_density(self.u_x,self.u_y,self.density,self.energy)
        next_u_x[:,:] = self.convection.get_next_u_x(self.u_x,self.u_y,self.density,self.energy)
        next_u_y[:,:] = self.convection.get_next_u_y(self.u_x,self.u_y,self.density,self.energy)

        self.energy[:,:] = next_energy
        self.density[:,:] = next_density
        self.u_x[:,:] = next_u_x
        self.u_y[:,:] = next_u_y
                

        self.pressure[:,:] = self.convection.get_pressure(self.energy)
        self.temperature[:,:] = self.convection.get_temperature(self.energy,self.density)
        
        
        """ Orientates the arrays """
        self.correct_density[:,:] = np.flip(self.density.T,0)
        self.correct_energy[:,:] = np.flip(self.energy.T,0)
        self.correct_u_x[:,:] = np.flip(self.u_x.T,0)
        self.correct_u_y[:,:] = -np.flip(self.u_y.T,0)
        self.correct_temperature[:,:] = np.flip(self.temperature.T,0)
        self.correct_pressure[:,:] = np.flip(self.pressure.T,0)
        
        
        return self.convection.dt



    def save_last_data(self):
        """
        Saves the last time point as .npy, which makes it 
        easier to analyze the data by another class.
        """
        np.save("pressure",self.pressure)
        np.save("temperature",self.temperature)
        np.save("density",self.density)
        np.save("energy",self.energy)
        np.save("ux",self.u_x)
        np.save("uy",self.u_y)


if __name__=="__main__":
    sim = Simulate(300,100,(0,12e6),(0,4e6))
    print(sim.step())
    vis = FVis.FluidVisualiser()
    
    
    """ For making Shock Waves """
#    vis.save_data(200,sim.step,rho=sim.correct_density,u=sim.correct_u_x,\
#                  w=sim.correct_u_y,P=sim.correct_pressure,\
#                  e=sim.correct_energy,T=sim.correct_temperature,sim_fps=1)
#    
#    vis.animate_2D('e',matrixLike=True,\
#                  snapshots=[5,10])
#    
    """ 
    For making Convection. For no convection, remove boxes in 
    distribute_initial_conditions_solar_surface in Convection2D
    """
    vis.save_data(2000,sim.step,rho=sim.correct_density,u=sim.correct_u_x,\
                  w=sim.correct_u_y,P=sim.correct_pressure,\
                  e=sim.correct_energy,T=sim.correct_temperature,sim_fps=.1)
    
    vis.animate_2D('dT',matrixLike=True,\
                  snapshots=[10,1990])
    

    
    sim.save_last_data()
    vis.delete_current_data()
