def cross_section(R_values,L_values,F_C_list,show_every=50,core_limit=0.995):
    import matplotlib.pyplot as plt
    import seaborn as sns

    # -------------------------------------------------------------------------------------------------------
    # Assumptions:
    # -------------------------------------------------------------------------------------------------------
    # * R_values is an array of radii [unit: R_sun]
    # * L_values is an array of luminosities (at each r) [unit: L_sun]
    # * F_C_list is an array of convective flux ratios (at each r) [unit: relative value between 0 and 1]
    # * n is the number of elements in both these arrays
    # * R0 is the initial radius
    # * show_every is a variable that tells you to show every ...th step (show_every = 50 worked well for me)
    # * core_limit = 0.995 (when L drops below this value, we define that we are in the core)
    # -------------------------------------------------------------------------------------------------------

    # Cross-section of star
    # (See https://stackoverflow.com/questions/9215658/plot-a-circle-with-pyplot and
    # https://stackoverflow.com/questions/27629656/label-for-circles-in-matplotlib for details)

    n = len(R_values)
    R0 = R_values[0]
    plt.figure()
    fig = plt.gcf() # get current figure
    ax = plt.gca()  # get current axis
    rmax = 1.2*R0
    ax.set_xlim(-rmax,rmax)
    ax.set_ylim(-rmax,rmax)
    ax.set_aspect('equal')	# make the plot circular
    j = show_every
    for k in range(0, n-1):
    	j += 1
    	if j >= show_every:	# don't show every step - it slows things down
    		if(L_values[k] > core_limit):	# outside core
    			if(F_C_list[k] > 0.0):		# convection
    				circR = plt.Circle((0,0),R_values[k],color='red',fill=False)
    				ax.add_artist(circR)
    			else:				# radiation
    				circY = plt.Circle((0,0),R_values[k],color='yellow',fill=False)
    				ax.add_artist(circY)
    		else:				# inside core
    			if(F_C_list[k] > 0.0):		# convection
    				circB = plt.Circle((0,0),R_values[k],color='blue',fill = False)
    				ax.add_artist(circB)
    			else:				# radiation
    				circC = plt.Circle((0,0),R_values[k],color='cyan',fill = False)
    				ax.add_artist(circC)
    		j = 0
    circR = plt.Circle((2*rmax,2*rmax),0.1*rmax,color='red',fill=True)		# These are for the legend (drawn outside the main plot)
    circY = plt.Circle((2*rmax,2*rmax),0.1*rmax,color='yellow',fill=True)
    circC = plt.Circle((2*rmax,2*rmax),0.1*rmax,color='cyan',fill=True)
    circB = plt.Circle((2*rmax,2*rmax),0.1*rmax,color='blue',fill=True)
    ax.legend([circR, circY, circC, circB], ['Convection outside core', 'Radiation outside core', 'Radiation inside core', 'Convection inside core']) # only add one (the last) circle of each colour to legend
    plt.legend(loc=2)
    plt.xlabel('')
    plt.ylabel('')
    plt.title('Cross-section of star')

    # Show all plots
    plt.show()
