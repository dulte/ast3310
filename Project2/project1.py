import numpy as np
import matplotlib.pyplot as plt


#from p0 import energy as calculate_nuclear_energy
from project0 import calculate_nuclear_energy
from scipy.interpolate import interp2d
import seaborn as sns

from cross_section import cross_section





class stellar_core:
    def __init__(self,filename="opacity.txt"):

        #Reads and interpolates the opacity file
        self.file = filename
        self.read_opacity()

        #Sets the constants needed for the simulation
        self.G = 6.67408e-11 #Gravtiational Constant
        self.k = 1.3806505e-23 #Boltzmann constant
        self.m_u = 1.660539040e-27 #Unit atomic mass
        self.a = 7.5657e-16 #Radiation Constnat
        self.sigma = 5.670367e-8 #Stephan-Boltzmann constant


        #Defines the constant mass fractons and mu
        X = 0.7 #Fraction of hydrogen
        Y = 0.29 #Fraction of helium
        Z = 0.01 #Fraction of metal
        self.mu = 1./(2*X+3*Y/4.+Z/2.) #Average Molecular Weight
        

        #The various constants for project 2
        self.Cp = 5/2.*self.k/(self.mu*self.m_u)
        self.delta = 1
        self.Hp = 1
        self.alpha = 1
        self.beta = 1
        self.rp = self.Hp*self.alpha
        self.lm = self.Hp*self.alpha
        self.geometric_factor = 2./self.rp
        self.U = 0
        self.g = 0


    #Reads the opacity file and interpolates it
    def read_opacity(self):
            self.opacity = []
            self.log_Rs = []
            self.log_Ts = []
            with open(self.file) as f:
                self.log_Rs = np.array(f.readline().split()[1:]).astype(np.float)
                f.readline() #Skips the empty second row
                for line in f.readlines():
                    words = line.split()
                    self.log_Ts.append(words[0])
                    self.opacity.append(words[1:])

            self.opacity = np.array(self.opacity)
            self.log_Ts = np.array(self.log_Ts).astype(np.float)

            self.opacity_interp2d = interp2d(self.log_Rs,self.log_Ts,self.opacity)



    def get_opacity(self,T,rho):
        #Makes R from rho and T. Takes the log_10 of R and T
        log_R = np.log10(rho*.001/(T*1e-6)**3)
        log_T = np.log10(T)

        #Gets log_10(kappa) in cgs units
        log_kappa = self.opacity_interp2d(log_R,log_T)

        #Returns kappa in SI
        return 10**log_kappa*0.1
    
    
    #updates U
    def get_U(self,T,kappa,rho):
        self.U = 64/3.*(self.sigma*T**3)/(kappa*rho**2*self.Cp)*\
                    np.sqrt(self.Hp/(self.g*self.delta))
        return self.U


    #Updates g
    def get_g(self,r,m):
        self.g = (m)*self.G/(r**2)
        return self.g
    
    
    #Function for the nablas
    def get_nabla_ad(self,P,T,rho):
        return P*self.delta/(T*rho*self.Cp)

    def get_nabla_stable(self,L,T,r,rho,kappa):
        return L*3*kappa*rho*self.Hp/(4*np.pi*r**2*16.*self.sigma*T**4)

    def get_nabla_star(self,nabla_ad,xi,rho,T):
        return xi**2 + self.geometric_factor*self.U/(self.lm)*xi + nabla_ad
        
    def get_nabla_p(self,nabla_star,xi):
        return nabla_star-xi**2

    
    #Function for solving xi
    def get_xi(self,nabla_ad,nabla_stable,rho,T,kappa):
        M = self.U/(self.lm**2)
        coefficients = []
        coefficients.append(1./M)
        coefficients.append(1)
        coefficients.append(self.geometric_factor*self.U/self.lm)
        coefficients.append((nabla_ad-nabla_stable))
        roots = np.roots(coefficients)

        real_roots = np.real(np.array([root for root in roots if np.imag(root)==0]))

        return abs(np.max(real_roots))



    #A function that calculates and returns everything that has to
    #do with convection
    def check_convection_and_calculate_dT_dm_nabla_and_flux(self,T,R,rho,L,M,kappa):
        P_gas = self.P_gas(rho,T)
        self.get_g(R,M)
        self.set_heigth_and_length_parameters(T,R)
        self.get_U(T,kappa,rho)
        nabla_ad = self.get_nabla_ad(P_gas,T,rho)
        nabla_stable = self.get_nabla_stable(L,T,R,rho,kappa)
        xi = self.get_xi(nabla_ad,nabla_stable,rho,T,kappa)

        nabla_star = self.get_nabla_star(nabla_ad,xi,rho,T)
        nabla_p = self.get_nabla_p(nabla_star,xi)

        Fc = self.get_Fc(rho,T,xi)
        Fr = self.get_Fr(T,kappa,rho,nabla_star)
        F_tot = Fc+Fr

        dT_dm = self.dT_dm(L,R,T,rho)


        if(nabla_stable>nabla_ad ):
            return -T*nabla_star/(self.Hp*4*np.pi*R**2*rho),Fc/F_tot,nabla_star,nabla_stable,nabla_ad
        else:
            nabla_star = nabla_stable
            return dT_dm,0,nabla_star,nabla_stable,nabla_ad


    #Functions returning the convective and radiative flux
    def get_Fc(self,rho,T,xi):
        return rho*self.Cp*T*np.sqrt(self.g*self.delta)*(self.Hp)**(-3./2)\
                    *(self.lm/2.)**2*xi**3

    def get_Fr(self,T,kappa,rho,nabla_star):
        return 16*self.sigma*T**4*nabla_star/(3*kappa*rho*self.Hp)

    
    #Calculate the varous parameters used in the calculation of nabla
    def set_heigth_and_length_parameters(self,T,r):
        self.Hp = (self.k/self.mu)*(T/(self.m_u*self.g))
        self.rp = self.Hp*self.alpha*self.beta
        self.lm = self.Hp*self.alpha
        self.geometric_factor = 2./r#self.rp



    def solve(self,step,L0,R0,M0,rho0,T0,adaptive=True):
        #Just changes the name to dm
        dm = step

        #Initiaze empty lists
        M = []; R = []; L = []; rho = []; T = []; P = []; epsilon = []; Fcs = []
        nabla_ads = []; nabla_stars = []; nabla_stables = []; PPI_energies = []
        PPII_energies = []

        #Appends the initial values to the empty lists
        M.append(M0)
        R.append(R0)
        L.append(L0)
        rho.append(rho0)
        T.append(T0)
        P.append(self.P(rho0,T0))
        
        ppi,ppii,total_energy = calculate_nuclear_energy(rho[-1],T[-1])
        PPI_energies.append(ppi)
        PPII_energies.append(ppii)
        epsilon.append(total_energy)

        kappa = self.get_opacity(T[-1],rho[-1])

        Fc,nabla_star,nabla_stable,nabla_ad = self.check_convection_and_calculate_dT_dm_nabla_and_flux(T[-1],R[-1],rho[-1],L[-1],M[-1],kappa)[1:]
        Fcs.append(Fc)
        nabla_ads.append(nabla_ad)
        nabla_stables.append(nabla_stable)
        nabla_stars.append(nabla_star)


        #Tolerence for the change dV/V, used for adaptive steps
        p = 1e-1
        
        
        #To keep track on who many timesteps have elapsed, and when to print
        i = 0 
        
        #How many timesteps between printing progess
        writing_freq = 1000

        while M[-1] >= 0:
            
            
            #The new adaptive step, able to increase and decrease
            if adaptive:
                #Makes dm more precise when out of convection zone
                if M[-1] <= 0.7*M0:
                    p = 1e-4
                
                df = abs(np.array([self.dr_dm(R[-1],rho[-1]), \
                     self.dP_dm(M[-1],R[-1]),self.dT_dm(L[-1],R[-1],T[-1],rho[-1])]))
    
                f = np.array([R[-1], P[-1], T[-1]])
                dm = -(p*f/df).min()
                
                #A minimum and maximum value for the step
                if abs(dm) > 1e26:
                    dm = -1e26
                elif abs(dm) < 5e19:
                    dm = -5e19


            #Integrates using first order Euler
            R.append(R[-1] + dm*self.dr_dm(R[-1],rho[-1]))
            P.append(P[-1] + dm*self.dP_dm(M[-1],R[-2]))
            L.append(L[-1] + dm*self.dL_dm(rho[-1],T[-1]))
 
            #Get the opacity
            kappa = self.get_opacity(T[-1],rho[-1])
            
            #Retrives the correct dT/dr
            dT_dm,Fc,nabla_star,nabla_stable,nabla_ad =\
                            self.check_convection_and_calculate_dT_dm_nabla_and_flux(\
                                        T[-1],R[-2],rho[-1],L[-2],M[-1],kappa)
                            
            T.append(T[-1] + dm*dT_dm)
            
            
            #Saves the fracion, and nablas
            Fcs.append(Fc)
            nabla_ads.append(nabla_ad)
            nabla_stables.append(nabla_stable)
            nabla_stars.append(nabla_star)


            #Updates the density
            rho.append(self.rho(P[-1],T[-1]))


            #Gets and saves the energies from chains
            ppi,ppii,total_energy = calculate_nuclear_energy(rho[-1],T[-1])
            PPI_energies.append(ppi)
            PPII_energies.append(ppii)
            epsilon.append(total_energy)
            
            
            #Updates the mass
            M.append(M[-1] + dm)
            #Updates the user on the progress
            if i%writing_freq == 0:
                print("Mass: %f8; dm: %e" %(M[-1]/M0,dm), end="               \r")

            i+=1
            
            #Checks if certain values are below zero to stop non-phyical behaviour
            if (R[-1]<=0.0*R0) or (L[-1] <= 0.0*L0) or (M[-1] <= 0.0*M0):
                print("At mass {} either radius, luminocity or mass is zero".format(M[-1]/M0))
                print("Mass: ",M[-1])
                print("R: ",R[-1])
                print("L: ",L[-1])
                
                #If this is so, the arrays, excluding the last element, is retuned
                return np.array(M[:-1]),np.array(R[:-1]),np.array(L[:-1]),\
                        np.array(rho[:-1]),np.array(T[:-1]),np.array(P[:-1]),\
                        np.array(epsilon[:-1]),np.array(Fcs[:-1]),\
                        np.array(nabla_ads[:-1]),np.array(nabla_stables[:-1]),\
                        np.array(nabla_stars[:-1]), np.array(PPI_energies[:-1]),\
                        np.array(PPII_energies[:-1])


        return np.array(M),np.array(R),np.array(L),np.array(rho),\
            np.array(T),np.array(P), np.array(epsilon),np.array(Fcs),\
            np.array(nabla_ads),np.array(nabla_stables),\
            np.array(nabla_stars),np.array(PPI_energies),np.array(PPII_energies)



    #Functions for the values and derivatives of the needed quantities
    def dr_dm(self,r,rho):
        return 1./(4*np.pi*r**2*rho)

    def dP_dm(self,m,r):
        return -self.G*m/(4*np.pi*r**4)

    def dL_dm(self,rho,T):
        eps = calculate_nuclear_energy(rho,T)[2]
        return eps

    def dT_dm(self,L,r,T,rho):
        kappa = self.get_opacity(T,rho)
        return -3*kappa*L/(256*np.pi**2*self.sigma*r**4*T**3)

    #I include both the gaseus and radiation pressure in one method
    def P(self,rho,T):
        return rho/(self.mu*self.m_u)*self.k*T + self.a/3.*T**4

    def P_gas(self,rho,T):
        return rho/(self.mu*self.m_u)*self.k*T

    def rho(self,P,T):
        return (P - self.a/3.*T**4)*(self.mu*self.m_u)/(self.k*T)













if __name__ == '__main__':
    #Initialization of the class
    sc = stellar_core()


    
    #Code for running the first sanity check
    """
    T = 0.9e6
    rho = 55.9
    R = 0.84*695700e3
    M = 0.99*1.989e30
    kappa = 3.98
    a = 1
    L = 3.828e26
    P = sc.P(rho,T)

    sc.get_g(R,M)
    sc.set_heigth_and_length_parameters(T,R)
    sc.get_U(T,kappa,rho)
    nabla_ad = sc.get_nabla_ad(P,T,rho)
    nabla_s = sc.get_nabla_stable(L,T,R,rho,kappa)
    xi = sc.get_xi(nabla_ad,nabla_s,rho,T,kappa)

    nabla_star = sc.get_nabla_star(nabla_ad,xi,rho,T)
    nabla_p = sc.get_nabla_p(nabla_star,xi)

    Fc = sc.get_Fc(rho,T,xi)
    Fr = sc.get_Fr(T,kappa,rho,nabla_star)

    print("nabla_stable ",nabla_s)
    print("U ",sc.U)
    print("Hp ",sc.Hp)
    print("xi ",xi)
    print("nabla_star ",nabla_star)
    print("nabla_stable ",nabla_s)
    print("nabla_p ",nabla_p)
    print("nabla_ad ",nabla_ad, 2./5)
    print(Fc/(Fr+Fc))
    print(Fr/(Fr+Fc))

    exit()
    """






    #Some values of the sun
    R_sun = 6.96e8          #Sun radius [m]
    L_sun = 3.846e26        #Sun luminosity [W]
    M_sun = 1.989e30        #Sun mass [kg]
    avg_rho = 1.408e3

    #The initial condition for the sanity check, and the ones I'll change to get a physical star
    M0 = 1.*M_sun
    L0 = 1.*L_sun
    R0 = 1.*R_sun
    rho0 = 1.42e-7*avg_rho
    T0 = 5770.
    M_step = -1e-6*M_sun




########################################


    #For Sanity check cross section and nabla
    """
    plt.semilogy(R/R_sun,nabla_stable,'r',label=r"$\nabla_{stable}$")
    plt.semilogy(R/R_sun,nabla_star,'b',label=r"$\nabla^*$")
    plt.semilogy(R/R_sun,nabla_ad,'g',label=r"$\nabla_{ad}$")
    plt.legend()
    plt.xlabel(r"$R/R_{\odot}$")
    plt.ylabel(r"$\log \nabla$")
    plt.title(r"Temperature Gradients $\nabla$")
    plt.show()

    cross_section(R/R_sun,L/L_sun,Fc)
    """

########################################

    #Initial conditions giving different physical results.

    #The result presented in the report:
    T0 *= .95
    R0 *= 1.65
    rho0 *=20.

########################################



    #Check for Rho(P(rho0,T0),T0) = rho0
    assert(abs(sc.rho(sc.P(rho0,T0),T0)-rho0) < 1e-5)


    #Actual simulation of the star
    M,R,L,rho,T,P,epsilon,Fc,nabla_ad,nabla_stable,nabla_star,PPI_energies,PPII_energies =\
                         sc.solve(M_step,L0,R0,M0,rho0,T0,adaptive=True)

    #Printouts of the varables which we want to go to zero.
    print("M: ",M[-1]/M0)
    print("R: ",R[-1]/R0)
    print("L: ", L[-1]/L0)


    #The index where L<0.995 to mark the border of the core.
    core_border = np.argmin(np.abs(L/L0 - 0.995))
    print("Core Reaches out to: ",R[core_border]/R0)
    
    
    #The index at 15% where the convection border should be found
    optimal_convection_border = np.argmin(np.abs(R/R0 - 0.85))



########################################


    #Plots the nablas
    plt.semilogy(R/R0,nabla_stable,'r',label=r"$\nabla_{stable}$")
    plt.semilogy(R/R0,nabla_star,'b',label=r"$\nabla^*$")
    plt.semilogy(R/R0,nabla_ad,'g',label=r"$\nabla_{ad}$")
    plt.axvline(x=R[optimal_convection_border]/R0,color="red",alpha=0.4,linestyle="--",label="Minimum Convection Border")
    plt.legend()
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log \nabla$")
    plt.title(r"Temperature Gradients $\nabla$")
    plt.show()
    
    #Plots the cross section
    cross_section(R/R_sun,L/L_sun,Fc,show_every=100)
    
    #Plots the energies
    plt.semilogy(R/R0,PPI_energies/epsilon,label="PPI")
    plt.semilogy(R/R0,PPII_energies/epsilon,label="PPII")
    plt.semilogy(R/R0,epsilon/(np.max(epsilon)),label=r"$\epsilon/\epsilon_{max}$")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel("Percentage of energy created by chain/Energy over Maximal Energy")
    plt.title("Energy Production of the Star")
    plt.legend()
    plt.show()
    
    
    #Plots the fraction of transport
    #Notice that since F_c and F_R are normalized, then F_R = 1 - F_C (laziness)
    plt.plot(R/R0,Fc,label=r"Convection")
    plt.plot(R/R0,1-Fc,label=r"Radiation")
    plt.axvline(x=R[optimal_convection_border]/R0,color="red",alpha=0.4,linestyle="--",label="Minimum Convection Border")
    plt.title("Fraction of Energy Being Transported by Radiation and Convection")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\frac{F_i}{F_{tot}}$")
    plt.legend()
    plt.show()



    #Plotting as a function of R, for the main simulations for the physical stars
    plt.plot(R/R0,M/M0)
    plt.title("Mass vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$M/M_0$")
    plt.show()
    plt.plot(R/R0,L/L0)
    plt.axvline(x=R[core_border]/R0,color="red",alpha=0.4,linestyle="--",label="Core Border")
    plt.title("Luminosity vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$L/L_0$")
    plt.legend()
    plt.show()
    plt.semilogy(R/R0,rho)
    plt.title("Density vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(\rho)$")
    plt.show()
    plt.plot(R/R0,T/1e6)
    plt.title("Temperature vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"T [MK]")
    plt.show()
    plt.semilogy(R/R0,P)
    plt.title("Pressure vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(P)$")
    plt.show()
    plt.semilogy(R/R0,epsilon)
    plt.title("Epsilon vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(\epsilon)$")
    plt.show()
