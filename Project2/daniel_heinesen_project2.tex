\documentclass[a4paper,norsk, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage[norsk]{babel}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{float}
\usepackage{amssymb}
\usepackage[dvips]{epsfig}
\usepackage[toc,page]{appendix}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}
\usepackage{titling}
\usepackage{marvosym }
%\usepackage{subcaption}
\usepackage[noabbrev]{cleveref}
\usepackage{cite}
\usepackage{changepage}
\usepackage{subfig}
\usepackage{graphicx}

\setlength{\droptitle}{-10em}   % This is your set screw

\setcounter{tocdepth}{2}

\lstset{language=c++}
\lstset{alsolanguage=[90]Fortran}
\lstset{alsolanguage=Python}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\title{AST3310 Project 2}
\author{Daniel Heinesen, daniehei}
\begin{document}
\maketitle

\section{Introduction}\label{sec:intro}
In the last project we successfully simulated a model of a stellar core. In this project we are looking to make a model that encompasses more of the star than just the core. The only way energy was transported in the core was through radiative transport, but in reality this is just but one of the ways energy is transported. Here we are going to add one more of the major ways of transport, \textit{conduction}. We want to make a star in which the inner $10\%$ is the core, and the outer $15\%$ is a convection zone. 

\section{Theory}
We are going to use mostly the same code as the last project, but add in a check to see whether the a certain layer of the star is convectively unstable, and in this case we are going to use a different $\frac{\partial T}{\partial m}$ than in the last project. We are going to start by finding the criteria for this instability, and the new $\frac{\partial T}{\partial m}$ we'll need to use.

\subsection{The different $\nabla$'s and instability}

The scenario we are going to simulate is that of a small parcel of gas in one layer of the star beginning to rise to the above layers. This parcel will then transport energy between layer. Before defining the instability and onset of the convection, we are going to introduce on of the most used notations this project, the temperature gradients

\begin{equation}\label{eq:nabla}
\nabla = \frac{\partial \ln T}{\partial \ln P} = \frac{P}{T}\frac{\partial T}{\partial P},
\end{equation}
$P$ being the pressure and $T$ the temperature. There are four such temperature gradients we will meet here. $\nabla_{stable}$, $\nabla^{*}$, $\nabla_{ad}$ and $\nabla_{p}$. $\nabla_{ad}$ is the temperature gradient of the parcel if it moves between the layer adiabatically, meaning that no energy is interchanged with the surroundings. $\nabla_{p}$ is the opposite, the gradient of the parcel if it interchange energy with the layers it moves through. $\nabla_{stable}$ and $\nabla^{*}$ are the temperature gradient for the star(the gas outside the parcel) if not convection is present and all the energy is transported by radiation  -- $\nabla_{stable}$ --, and the actual gradient of the star, with or without convection -- $\nabla^{*}$. Notice that we want $\nabla_{star} = \nabla_{stable}$ if we only have radiative transport. The $\nabla$'s are important both because we can write the criterion for convection using them, and the resulting $\frac{\partial T}{\partial m}$.\\

We can now introduce the instability criterion found in the lecture notes

\begin{equation}
\frac{\partial T}{\partial r} < \left(\frac{\partial T}{\partial r}\right)_{ad} - \frac{T}{\mu}\left(\frac{\partial \mu}{\partial r}\right)_{ad} +\frac{T}{\mu}\frac{\partial \mu}{\partial r}.
\end{equation}

Where $\frac{\partial T}{\partial r}$ is the temperature gradient of the star with radiation transport, and $\left(\frac{\partial T}{\partial r}\right)_{ad}$ is the gradient of the parcel moving adiabatically. $\left(\frac{\partial \mu}{\partial r}\right)_{ad}$ is the change in mean molecular weight as the parcel expands or contracts, and $\frac{\partial \mu}{\partial r}$ is the change in mean molecular weight at the different layers of the star. We are going to assume that the mean molecular weight stays the same in both of the cases. This leaves us with the criterion 
\begin{equation}
\frac{\partial T}{\partial r} < \left(\frac{\partial T}{\partial r}\right)_{ad}.
\end{equation}
If we rewrite this using the $\nabla$'s described above we get
\begin{equation}\label{eq:IC}
\nabla_{stable} > \nabla_{ad}.
\end{equation}
This is the instability criterion we are going to use in the simulation.\\

Before we can actually use this criterion, we need to know how to find the $\nabla$'s. $\nabla_{ad}$ is given from the lecture notes as
\begin{equation}\label{n_ad}
\nabla_{ad} = \left(\frac{P}{T}\frac{\partial T}{\partial P}\right)_s = \frac{
P\delta}{T\rho c_p},
\end{equation}
where $\rho$ is the density, $c_p$ is the specific heat for constant pressure (given below), and $\delta$ is given as
\begin{equation}
\delta = -\left(\frac{\partial \ln \rho}{\partial \ln T}\right)_P = \frac{T}{V}\left(\frac{\partial V}{\partial T}\right)_P,
\end{equation}
and is equal to $1$ for an ideal gas -- which we are assuming for the star. The specific heat is given as 
\begin{equation}
c_p = \frac{5k_B}{2\mu m_u},
\end{equation}
where $k_B$ is Boltzmann's constant and $m_u$ is the mean molecular mass. 
We now have one of the $\nabla$'s. But to find the rest, we have to do some more smart tricks.

\subsection{Finding $\xi$ and the remaining $\nabla$'s}
To start we are going to introduce the energy flux of the radiative, $F_R$, and convective, $F_C$, transports, which are found in the lecture notes,

\begin{equation}
F_C = \rho c_p T \sqrt{g\delta}H_p^{-3/2}\left(\frac{1}{2}l_m\right)^2(\nabla^{*}-\nabla_p)^{3/2}, \qquad F_R = \frac{16\sigma T^4}{3\kappa\rho H_p}\nabla^*,
\end{equation}
and how they relate
\begin{equation}\label{eq:FRFC}
F_R + F_c = \frac{16\sigma T^4}{3\kappa\rho H_p}\nabla_{stable}.
\end{equation}
Here $\sigma$ is the Stefan-Boltzmann constant, $\kappa$ the opacity, $g$ is the gravitational acceleration at the given radius, $l_m$ is the mixing length, and $H_p$ is the pressure scale length given as
\begin{equation}\label{eq:Hp}
H_p = -P\frac{\partial r}{\partial P}=  \frac{k_B T}{\mu m_u g}.
\end{equation}
We can now combine these 
\begin{equation}
\frac{16\sigma T^4}{3\kappa\rho H_p}\nabla^* + \rho c_p T \sqrt{g\delta}H_p^{-3/2}\left(\frac{1}{2}l_m\right)^2(\nabla^{*}-\nabla_p)^{3/2} = \frac{16\sigma T^4}{3\kappa\rho H_p}\nabla_{stable}
\end{equation}
\begin{equation}
\Rightarrow (\nabla^{*}-\nabla_p) = \left[\frac{16\sigma T^3}{3\kappa\rho^2 }\frac{\sqrt{H_p}}{c_p\sqrt{g\delta}\left(\frac{1}{2}l_m\right)^2}(\nabla_{stable} - \nabla^*)\right]^{2/3} = \left[\frac{U}{l_m^2}(\nabla_{stable} - \nabla^*)\right]^{2/3},
\label{eq:n_star-n_p}
\end{equation}
where
\begin{equation}
U = \frac{64\sigma T^3}{3\kappa \rho^2 c_p}\sqrt{\frac{H_p}{g\delta}}.
\label{eq:U}
\end{equation}

We are then going to use another relation given in the lecture notes
\begin{equation}
(\nabla_p - \nabla_{ad}) = \frac{32\sigma T^3}{3\kappa \rho^2 c_p v}\frac{S}{Qd}(\nabla^* - \nabla_p),
\end{equation}
where $\Gamma = S/Qd$ -- $\Gamma$ just being the shorthand I will be using onwards -- is a geometrical factor\footnote{ $S$ is the surface of the parcel, $d$ the diameter and $Q$ is the surface facing the direction of motion.} and is for a spherical parcel of radius $r_p$ equal to $2/r_p$. $v$ is the velocity of the parcel and is given as
\begin{equation}
v = \sqrt{\frac{g\delta l_m^2	}{4H_p}} (\nabla^* - \nabla_p)^{1/2}.
\end{equation}
We can combine this with the identity
\begin{equation}
(\nabla_p - \nabla_{ad}) = (\nabla^* - \nabla_{ad}) - (\nabla^* - \nabla_{p})
\end{equation}
to get
\begin{equation}
(\nabla^* - \nabla_{ad}) - (\nabla^* - \nabla_{p}) = \frac{32\sigma T^3}{3\kappa \rho^2 c_p v}\frac{S}{Qd}(\nabla^* - \nabla_p) 
\end{equation}
\begin{equation}
= \frac{64\sigma T^3}{3\kappa \rho^2 c_p }\sqrt{\frac{H_p}{g\lambda l_m^2}}\frac{S}{Qd}(\nabla^* - \nabla_p)^{1/2} = \Gamma \frac{U}{l_m} \xi,
\end{equation}
where $\xi = (\nabla^* - \nabla_p)^{1/2}$ is a variable we are going to use to find all the $\nabla$'s. This gives us the second order polynomial
\begin{equation}
\xi^2 + \Gamma \frac{U}{l_m} \xi - (\nabla^* - \nabla_{ad}) = 0.
\end{equation}
This can easily be solved, to find
\begin{equation}
\xi = - \frac{\Gamma U}{2l_m}\pm\frac{1}{2}\sqrt{\left(\frac{\Gamma U}{l_m}\right)^2 + 4(\nabla^* - \nabla_{ad})} = - \frac{\Gamma U}{2l_m}\pm\sqrt{\left(\frac{\Gamma U}{2l_m}\right)^2 + (\nabla^* - \nabla_{ad})}.
\end{equation}
We have the relation $\nabla_{ad} < \nabla_{p} < \nabla^* < \nabla_{stable}$, so we know that $\nabla^* > \nabla_p$, so $\nabla^* - \nabla_p > 0$ and thus $\xi > 0$, giving us only one possible solution, namely
\begin{equation}
\xi = - \frac{\Gamma U}{2l_m}+\sqrt{\left(\frac{\Gamma U}{2l_m}\right)^2 + ((\nabla^* - \nabla_{ad})}.
\label{eq:xi}
\end{equation}
We are not able to solve this yet. We have to eliminate $\nabla^*$. We start by isolating $\nabla^*$ from \eqref{eq:xi}
\begin{equation}\label{eq:n_star}
\nabla^* = \xi^2 + \frac{\Gamma U}{l_m}\xi + \nabla_{ad}.
\end{equation}
We can so insert this into \eqref{eq:n_star-n_p} to get
\begin{equation}
\Rightarrow (\nabla^{*}-\nabla_p) = \xi^2 = \left[\frac{U}{l_m^2}(\nabla_{stable} - \nabla^*)\right]^{2/3} = \left[\frac{U}{l_m^2}(\nabla_{stable} - \xi^2 - \frac{\Gamma U}{l_m}\xi - \nabla_{ad})\right]^{2/3}
\end{equation}
\begin{equation}
\Rightarrow \frac{l_m^2}{U}\xi^3 = \nabla_{stable} - \xi^2 - \frac{\Gamma U}{l_m}\xi - \nabla_{ad}
\end{equation}
Giving us the third order polynomial
\begin{equation}
\frac{l_m^2}{U}\xi^3 + \xi^2 + \frac{\Gamma U}{l_m}\xi + (\nabla_{ad} - \nabla_{stable}) = 0.
\label{eq:xi3}
\end{equation}

We are still missing $\nabla_{stable}$, but use the fact that the total energy flux must obey
\begin{equation}
F_{tot} = \frac{L}{4\pi r^2}.
\end{equation}
Combining this with \eqref{eq:FRFC} we see that
\begin{equation}\label{eq:n_stable}
\nabla_{stable} = \frac{3\kappa\rho H_p L}{64\pi r^2 \sigma T^4}.
\end{equation}
This means that we can find all the $\nabla$'s:
\begin{enumerate}
\item Calculate $\nabla_{ad}$ from \eqref{n_ad} and $\nabla_{stable}$ from \eqref{eq:n_stable}.
\item Solve \eqref{eq:xi3} to find $\xi$.
\item Calculate $\nabla^*$ from \eqref{eq:n_star}.
\item Lastly, calculate $\nabla_p = \nabla^* - \xi^2$.
\end{enumerate}

We now have all we need to check the instability criterion \eqref{eq:IC}.




\subsection{The new $\frac{\partial T}{\partial m}$}
We now know when convection sets in. But, what effect does convection have on the star. To see this, lets look at the definition of $\nabla$ \eqref{eq:nabla}, and use the definition $H_p$\eqref{eq:Hp}:
\begin{equation}
\nabla =  \frac{P}{T}\frac{\partial T}{\partial P} = -\frac{H_p}{T}\frac{\partial T}{\partial r} = -\frac{H_p}{T}\frac{\partial T}{\partial m} \frac{\partial m}{\partial r}.
\end{equation}
Isolating $\partial T/\partial m$ and using the definition of $\partial r/\partial m$ from project 1 we find
\begin{equation}
\frac{\partial T}{\partial m} = -\frac{T}{4H_p\pi r^2\rho} \nabla.
\end{equation}
If no convection is present, then $\nabla = \nabla_{stable}$, in which case this reduces to same $\partial T/\partial m$ as in project 1. But, if there is convection, then $\nabla = \nabla^*$\footnote{Note that $\nabla = \nabla^*$ is always true, but as noted, if there is no convection, then $\nabla^* = \nabla_{stable}$}. So if in a layer of the star, the instability criterion triggers and convection starts, then the temperature in this layer is updated using
\begin{equation}\label{eq:dTdm}
\frac{\partial T}{\partial m} = -\frac{T}{4H_p\pi r^2\rho} \nabla^*.
\end{equation}

\subsection{Reaction Rates}
We also want to look at the which reaction chain happens where. We expect PPI to be dominant in the outer layers with lower temperature, and PPII and PPIII to be dominant for higher temperatures -- like in the core. We are going to take PPII and PPIII to be the same reaction chain. Finding what happens where is fairly straight forward, but PPI and PPII/III share one reaction namely $3 ^1_1\text{H} \rightarrow ^3_2 \text{He} + e^- + \nu_e$, where the $^3_2$He is used in both of the chains. We can then see that the fraction of energy of this reaction that goes into PPI is $r_{33}/(r_{33} + r_{34})$, where $r_{33}$ is the reaction rate of PPI, and $r_{34}$ is the reaction rate of the first reaction in PPII/III. Then the fraction of energy going into PPII/III is $r_{34}/(r_{33} + r_{34})$. Thus we get that the energy outputted by the proton-proton reactions are
\begin{equation}\label{eq:PPenergies}
\epsilon_{\text{PPII}} = \frac{r_{33}}{r_{33} + r_{34}}\cdot\epsilon_{pp} + \epsilon_{33},\qquad  \epsilon_{\text{PPII/III}} = \frac{r_{34}}{r_{33} + r_{34}}\cdot\epsilon_{pp} + \epsilon_{34} + \epsilon_{e7} + \epsilon_{17} + \epsilon_{17'}
\end{equation}


\section{Method}
The code used here will be more or less the same as in project 1. The main difference is that before the temperature is updated, all the $\nabla$'s are calculated, the instability criterion \eqref{eq:IC} is check and if triggered the new $\partial T/\partial r$ \eqref{eq:dTdm} is used. The code from project 0 is change to be able return the energy for the different reaction chains \eqref{eq:PPenergies} and the total energy.\\

There are two parameters mentioned in the theory section which is not defined, namely $l_m$ and $r_p$. The mixing length $l_m$ is just taken to be
\begin{equation}
l_m = \alpha H_p
\end{equation}
where $\alpha$ is a free parameter between $1/2$ and $2$. If not stated otherwise, this parameters is chosen to be $1$. The parcel radius $r_p$ can be defined in two ways. The first is that the parcel is the whole layer we are looking at, in which case $r_p = dr$. An easier way is to define it in the same way we defined $l_m$, as
\begin{equation}
r_p = \beta H_p,
\end{equation}
again with $\beta$ being a free parameter, by default taken to be $1$ unless otherwise stated. We will use the second method.\\

The code for the adaptive $dm$ was also changed. Now it has the ability to increase. The code in project 1 would only decrease the mass step, but this would make the code used here incredibly slow. $dm$ have also gotten a maximum value of $10^{26}$, and a minimum value -- see sec. \ref{sec:problems}.



\subsection{Changes/Corrections to Project 1}
There were two minor changes done to the code taken from project 1. The first was the mean molecular weight. The ionization of Helium is now corrected, and the new molecular weight is now

\begin{equation}
\mu = \frac{1}{2X + 3/4 Y + Z/2}.
\end{equation}

The other change was is the rates of PPI and PPII. The rates $r_{33}$ and $r_{34}$ was inhibited to use more $^3$He than was produced by $r_{pp} \geq r_{33} + r_{34}$. This was wrong since $r_{33}$ uses 2 $^3$He. The correct inhibition is therefore

\begin{equation}
r_{pp} \geq 2r_{33} + r_{34}
\end{equation}

Both of these correction are now in the code used in this project.

\subsection{Finding the Optimal Initial Conditions}
As mentioned in the introduction \ref{sec:intro}, want a star with the inner $10\%$ of the radius being the core, and the outer $15\%$ of the radius being a convection zone. Including this, we are after the same goals as the last project. To try to accomplish this we are going to tweak $T_0$, $\rho_0$ and $R_0$. We are going to start by tweaking them one at the time, and seeing what effect this has. With what we have learned from this, we are going to try to tweak all of them to get the optimal result.
\section{Results}
\subsection{Sanity Checks}
For the first sanity check some of the calculates made by the code are compared with example 5.1 from the lecture notes.




\begin{table}[ht!]
\centering
\begin{adjustwidth}{-2cm}{}
\begin{tabular}{c|c|c|c|c|c|c|c|c|c}
& $\nabla_{stable}$ & $\nabla_{ad}$ & $\nabla_{p}$ & $\nabla^*$ & $H_p$ & $U$ & $\xi$ & $F_C/(F_C + F_R)$& $F_R/(F_C + F_R)$\\
\hline
Ex 5.1 & $3.26$ & $2/5$ & $0.4$ & $0.40001$ & $32.5$Mm & $5.94\cdot10^{5}$ & $1.175\cdot10^{-3}$&$0.88$&$0.12$\\
Program& $3.16$&$0.400098$&$0.400098$&$0.400099$&$31.6$Mm&$6.02\cdot10^5$&$1.185\cdot10^{-3}$&$0.873$&$0.127$
\end{tabular}
\caption{Values from example 5.1 compared with values calculated by the simulation. This was done with $T = 0.9\cdot10^6$K, $\rho = 55.9$kg m$^{-3}$, $R = 0.84R_{\odot}$, $M=0.99M_{\odot}$,$\kappa = 3.98$m$^2$kg$^{-1}$ and $\alpha = 1$. The main difference is that the example uses $\mu = 0.6$, while the program calculates $\mu = 1/(2X + 3/4 Y + Z/2) = 0.616$. Still, with if $\mu = 0.6$ is hardcoded the $\nabla$'s undershoots a bit, but is closer that shown here.}\label{tab:san1}
\end{adjustwidth}
\end{table}
As shown in table \ref{tab:san1} the values from the program is quite close, but deviates somewhat from the values written in the example. This is mostly due to different $\mu$, but the exact values of other constants like $k_B$,$G$, etc will also make some difference. But we see that the relationship $\nabla_{ad} < \nabla_{p} < \nabla^* < \nabla_{stable}$ still holds (the fact that $\nabla_{ad} < \nabla_{p}$ is sadly lost in the computers handling of decimal points, but is necessarily the case due to the equations used to calculate them).

The next sanity check is to look at a cross section of the star and the $\nabla$'s for the initial conditions given in the project.

\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.45\textwidth]{nabla_sanity}}\quad
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_sanity}}
\caption{Cross section of the star (b). Here we can see a layer of convection on the very edge of the star, as we would expect. There is also a convection layer at the center, inside the core. (a) show the $\nabla$'s. We can see that $\nabla_{stable }> \nabla_{ad}$ at the very start ($R/R_{\odot} \approx 1$) and at the very end ($R/R_{\odot} \approx 0.07$). This coincides with where we see the convection zones in the cross section. This simulation was done with $L_0 = L_{\odot}$, $R_0 = R_{\odot}$, $M_0 = M_{\odot}$, $\rho_0 = 1.42\cdot10^{-7}\bar{\rho}_{\odot}$ and $T_0 = 5770$K. Here the whole simulation was run with $p=10^{-3}$ and a minimum $dm$ of $5\cdot 10^{16}$ (see sec. \ref{sec:problems})}\label{fig:san2}
\end{figure}

We can see from figure \ref{fig:san2} that our code gets mostly the same result as the sanity check. $\nabla_{stable}$ seems to go a bit higher at the very end in the plot from sanity check, but this is most likely due to different precision in the values of the initial conditions and the size of $dm$. I tried using more decimal points in my definition of $R_{\odot}$ and $M_{\odot}$, and this actually lead $\nabla_{stable}$ to be even smaller at $R/R_{\odot} \approx 0.07$. \\

Now that we know that the code works, we can proceed to find the optimal initial conditions.

\subsection{Finding the Optimal Initial Conditions} 
Looking at \eqref{eq:n_stable} we can see that all the initial conditions, $T$, $\rho$ and $R$, will affect the convection zone. We are going to start by tweaking $\rho_0$, $T_0$ and $R_0$ to see what effect this has on the star. We will from here on call the initial conditions given in the sanity check $L_{SC} = L_{\odot}$, $R_{SC} = R_{\odot}$, $M_{SC} = M_{\odot}$, $\rho_{SC} = 1.42\cdot10^{-7}\bar{\rho}_{\odot}$ and $T_{SC} = 5770$K \footnote{\textit{SC} for Sanity Check.}, and the initial conditions we will tweak will be multiples of these.

\subsubsection{Changing the Initial Conditions}


\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_2R}}\quad
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_05R}}
\caption{Cross section of the star using the same initial conditions as in fig. \ref{fig:san2}, but with $R_0 = 2R_{SC}$ in (a) and $R_0 = 0.5R_{SC}$ in (b). We can see that in (a) the convection zone near the surface is larger, but the core is smaller, while in (b) the convection zone is almost gone, and something went below $0$ before reaching the center.}\label{fig:2R}
\end{figure}

We are going to start be changing the initial radius. We see from fig. \ref{fig:2R} that to get a larger convection zone, we ought to use a larger initial radius. The core will be too small, so we need to find some other parameter -- $T$ or $\rho$ -- which can compensate for this. We then go on to do the same to the temperature as we did to the radius.

\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_2T}}\quad
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_05T}}
\caption{Cross section of the star using the same initial conditions as in fig. \ref{fig:san2}, but with $T_0 = 2T_{SC}$ in (a) and $T_0 = 0.5T_{SC}$ in (b). We can see that in (a) the star looks more or less the same as in fig. \ref{fig:san2}, with mayby a somewhat smaller convection zone at the edge. The same mostely goes for (b), but we can see that surface convection zone is a bit larger.}\label{fig:2T}
\end{figure}

We see from fig. \ref{fig:2T} that to get a wider convection zone near the surface we need to lower the temperature. I have to mention that with $T_0 \leq 0.8\cdot T_{SC}$ my program begins to give Runtime warnings. I do not know if these actually impacts the result, but to be sure, we will only use $T_0 > 0.8\cdot T_{SC}$.

The last parameter to change is the density $\rho$.

\begin{figure}[ht!]
\centering
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_2rho}}\quad
\subfloat[][]{\includegraphics[width=.45\textwidth]{cross_05rho}}
\caption{Cross section of the star using the same initial conditions as in fig. \ref{fig:san2}, but with $\rho_0 = 2\rho_{SC}$ in (a) and $\rho_0 = 0.5\rho_{SC}$ in (b). As for the radius, we can see that in (a) the surface convection zone is larger, but unlike a larger raduis, the core is more or less the same as in the sanity check, maybe minisculely enlarged. For (b) we have a smaller surface convection zone, and an seemingly untouched core.}\label{fig:2rho}
\end{figure}

We see from fig. \ref{fig:2rho} that an increase in the density does exactly what we are after. But, as with the sanity check, the simulation cuts off before reaching the center. This seemed not to happen when increasing radius \ref{fig:2R}, but rather, this had the effect of closing the gap to the center. For all these simulations, the whole simulation was run with $p=10^{-3}$ and a minimum $dm$ of $5\cdot 10^{16}$ (see sec. \ref{sec:problems}).

\subsubsection{The Optimal Plots}\label{sec:opt}
Knowing all of this, we can begin to guess how the initial conditions we want look like. We should decrease the temperature a bit, but not too much. To get a larger surface convection zone without effecting the core too much, we should increase the density quite a bit. To enlarge the surface convection zone even more, and to close the gap to the center, we should also increase the radius. \\

The final optimal initial conditions were $T_0 = 0.95\cdot T_{SC}$, $\rho_0 = 20\cdot\rho_{SC}$ and $R_0 = 1.65\cdot R_{SC}$. The end values for the mass, radius and luminosity were: $M/M_0 = 0.04$, $R/R_0 = 0.002$ and $L/L_0 = 6.0\cdot10^{-5}$, which are within the values we were looking for. The resulting plots can be seen in fig. \ref{fig:nabla_frac}, \ref{fig:eps_frac} and \ref{fig:rest}.


\begin{figure}[H]
\centering
\subfloat[][]{\includegraphics[width=.33\textwidth]{cross_final}}
\subfloat[][]{\includegraphics[width=.33\textwidth]{nabla_final}}
\subfloat[][]{\includegraphics[width=.33\textwidth]{frac_final}}
\caption{(a) shows the cross section of a star which fullfills all the requirements we were looking for \eqref{sec:intro}. The core reaches out to about $15\%$ of $R_0$, while the surface convection zone reaches about $18\%$ of $R_0$.(b) shows the temperature gradients for our final model, while (c) is the fraction of the flux carried by convection and/or radiation. In both (b) and (c) the minimum border of the convection -- $15\%$ of $R_0$ -- is marked. We see from (b) the $\nabla_{stable} > \nabla_{ad}$ is beyond this border, indicating that our convection zone is large enough. (c) shows that convection is the main transport of energy in the convection zone, and that radiation takes over after that. We can also see from these plots that the convection zone in the core is gone. The final initial conditions were $T_0 = 0.95\cdot T_{SC}$, $\rho_0 = 20\cdot\rho_{SC}$ and $R_0 = 1.65\cdot R_{SC}$. }\label{fig:nabla_frac}
\end{figure}


\begin{figure}[H]
\centering
\subfloat[][]{\includegraphics[width=.33\textwidth]{eps__final}}
\subfloat[][]{\includegraphics[width=.33\textwidth]{eps__final_close}}
\subfloat[][]{\includegraphics[width=.33\textwidth]{mass}}
\caption{(a) and (b) show the fraction of the energy produced by PPI and PPII/III, and normalized total energy. (b) is a closeup, so to see the behaviour of the fractions. We see that, as expected, PPI is dominant near the edge, and that PPII/III takes over as we get near the core. The total energy productions is by far the largest in the core, where the temperature is high. (c) shows that the mass goes more or less to $0$ at the center. This plot looks quite similar to the one from project 1, which is what we want.}\label{fig:eps_frac}
\end{figure}

\begin{figure}[H]
\centering
\subfloat[][]{\includegraphics[width=.4\textwidth]{temp}}\quad
\subfloat[][]{\includegraphics[width=.4\textwidth]{luminosity}}\quad\\
\subfloat[][]{\includegraphics[width=.4\textwidth]{pressure}}\quad
\subfloat[][]{\includegraphics[width=.4\textwidth]{rho}}
\caption{The main parameters of the star evolves much the same way as they did in project 1. The main difference is the luminosity, which fall off faster then in project 1. If this has anything to do with the convection or the choice of initial conditions in not known.}\label{fig:rest}
\end{figure}

\section{Discussion and Conclusion}
We can see from sec. \ref{sec:opt} that we managed to get the kind of star we were looking after. We managed to have the mass, radius and luminosity with in $5\%$ of the initial conditions, in the same way as in project 1. More important for this project, we got a large enough surface convection zone and core -- see fig. \ref{fig:nabla_frac} and \ref{fig:rest}. We also see from fig. \ref{fig:rest} and \ref{fig:eps_frac} that near the center of the core a higher production of energy caused by PPII/III begins. This causes the energy, density and pressure to sharply increase, while the luminosity plummets to $0$. This luminosity decrease was more of a "mystery" in the last project, but having looked at the fraction of PPI and PPII/III we see that the sudden increase in energy due to PPII/III is the reason we see this effect. We did also manage to avoid a convection zone in the core, which we had in the sanity check.\\

\begin{figure}[H]
\centering
\includegraphics[scale=0.3]{nasa_sun}
\caption{This is an illustration of the cross section of the sun, taken from \url{https://www.nasa.gov/feature/goddard/2017/esa-nasa-s-soho-reveals-rapidly-rotating-solar-core}}\label{fig:sun}
\end{figure}

If we look at a real cross section of the sun, fig. \ref{fig:sun}, we see that the general look is more or less the same. The sun has a larger convection zone and core. So if we want to simulate the actual sun, we need to find initial conditions that increase the convection zone and core. Other then their sizes, the convection zone and core looks like the sun. We do of course lack the chromo- and photosphere found on the sun. 

\subsection{Problems}\label{sec:problems}
One of the main problems was the run time of the program. The calculations done near the surface needed a very small $dm$. To make the program go faster I increased the factor controlling the adaptive steps to $p = 10^{-1}$, and made the minimum value of $dm$ be $5\cdot 10^{16}$. Still, with these modifications, the program had an average run time of $10-15$ min. This meant it took quite a long time to search for optimal initial values. To combat this I set the minimum step size to $5\cdot 10^{19}$, while looking for the initial values. Having found the these initial value, I decreased the minimum step size, and ran the program again with the same initial values to see if the result was the same. Fortunately, they were. Since there is an extreme decrease in luminosity near the core, it was important to make sure $dm$ was small enough here. I therefore decreased $p$ again after I was sure that we were out of the outer convection zone. $p$ was then decreased to $p = 10^{-4}$. This is the reason the yellow radiation zone has few lines. Other than that, there were no real problems with the analytical part, but a few programming hiccups, mostly due to sleep deprivation. The worst being that I checked the second to last values of $M$, $R$ and $L$ to see if they converged to $0$, instead of the last values. This meant that I didn't find values which made the luminosity go to $0$ before 04:00 on Sunday morning...

\section{Code and Acknowledgement}
The code was programmed in Python 3.5, and tested on Windows 10 and Ubuntu. The main program is \verb|project2.py| and \verb|project0.py|. The cross section code we got from the Professor was made into a function, and can be found as the attachment \verb|cross_section.py|.\\

I would like to thank Julie Thingwall, a dear friend who took the course last year, who helped me with the new version of the adaptive steps, and general discussion.

\end{document}

