import numpy as np
import matplotlib.pyplot as plt

from project0 import calculate_nuclear_energy
from scipy.interpolate import interp2d





class stellar_core:
    def __init__(self,filename="opacity.txt"):

        #Reads and interpolates the opacity file
        self.file = filename
        self.read_opacity()

        #Sets the constants needed for the simulation
        self.G = 6.67408e-11 #Gravtiational Constant
        self.k = 1.3806505e-23 #Boltzmann constant
        self.m_u = 1.660539040e-27 #Unit atomic mass
        self.a = 7.5657e-16 #Radiation Constnat
        self.sigma = 5.670367e-8 #Stephan-Boltzmann constant
        
        #Defines the constant mass fractons and mu
        X = 0.7 #Fraction of hydrogen
        Y = 0.29 #Fraction of helium
        Z = 0.01 #Fraction of metal
        self.mu = 1/(2*X+3*Y/4.+Z/2.) #Average Molecular Weight

    #Reads the opacity file and interpolates it
    def read_opacity(self):
            self.opacity = []
            self.log_Rs = []
            self.log_Ts = []
            with open(self.file) as f:
                self.log_Rs = np.array(f.readline().split()[1:]).astype(np.float)
                f.readline() #Skips the empty second row
                for line in f.readlines():
                    words = line.split()
                    self.log_Ts.append(words[0])
                    self.opacity.append(words[1:])

            self.opacity = np.array(self.opacity)
            self.log_Ts = np.array(self.log_Ts).astype(np.float)

            self.opacity_interp2d = interp2d(self.log_Rs,self.log_Ts,self.opacity)



    def get_opacity(self,T,rho):
        #Makes R from rho and T. Takes the log_10 of R and T
        log_R = np.log10(rho*.001/(T*1e-6)**3)
        log_T = np.log10(T)

        #Gets log_10(kappa) in cgs units
        log_kappa = self.opacity_interp2d(log_R,log_T)

        #Returns kappa in SI
        return 10**log_kappa*0.1



    def solve(self,step,L0,R0,M0,rho0,T0,adaptive=True):
        #Just changes the name to dm
        dm = step

        #Initiaze empty lists
        M = []; R = []; L = []; rho = []; T = []; P = []; epsilon = [];

        #Appends the initial values to the empty lists
        M.append(M0)
        R.append(R0)
        L.append(L0)
        rho.append(rho0)
        T.append(T0)
        P.append(self.P(rho0,T0))
        epsilon.append(calculate_nuclear_energy(rho[-1],T[-1]))

        #Makes arrays for the use in the adaptive steps
        dV = np.zeros(4)
        V = np.zeros(4)

        #Tolerence for the change dV/V, used for adaptive steps
        p = 1e-2
        
        while M[-1] >= 0:

            #Integrates using first order Euler
            R.append(R[-1] + dm*self.dr_dm(R[-1],rho[-1]))
            P.append(P[-1] + dm*self.dP_dm(M[-1],R[-2]))
            L.append(L[-1] + dm*self.dL_dm(rho[-1],T[-1]))
            T.append(T[-1] + dm*self.dT_dm(L[-2],R[-2],T[-1],rho[-1]))

            rho.append(self.rho(P[-1],T[-1]))
            
            epsilon.append(calculate_nuclear_energy(rho[-1],T[-1]))

            M.append(M[-1] + dm)


            #Handles adaptive steps
            if adaptive:

                dV[0] = R[-2]-R[-1]
                dV[1] = P[-2]-P[-1]
                dV[2] = L[-2]-L[-1]
                dV[3] = T[-2]-T[-1]

                V[0] = R[-1]
                V[1] = P[-1]
                V[2] = L[-1]
                V[3] = T[-1]

                dV_V = np.abs(dV)/V
                
                #Checks if the relative change is too large, and changes dm
                if (True in (dV_V>p)):
                    g = np.abs(dV)/np.abs(dm)
                    dm = -np.min(p*V/g)
                    #If dm is below a certain value, the simulation would run
                    #for a very long time, so this is prohibited
                    if abs(dm) < 1e20:
                        print("Reached too small dm, stoping adaptive steps.")
                        adaptive = False
                    
            
            #Updates the user on the progress
            print("Mass: %f8; dm: %e" %(M[-1]/M0,dm), end="               \r")


            #Checks if certain values are below zero to stop non-phyical behaviour
            if (R[-1]<=0.0*R0) or (L[-1] <= 0.0*L0) or (M[-1] <= 0.0*M0):
                print("At mass {} either radius, luminocity or mass is zeros".format(M[-1]/M0))
                print("Mass: ",M[-1])
                print("R: ",R[-1])
                print("L: ",L[-1])
                #If this is so, the arrays, excluding the last element, is retuned
                return np.array(M[:-1]),np.array(R[:-1]),np.array(L[:-1]),\
                        np.array(rho[:-1]),np.array(T[:-1]),np.array(P[:-1]),\
                                 np.array(epsilon[:-1])


        return np.array(M),np.array(R),np.array(L),np.array(rho),\
                            np.array(T),np.array(P), np.array(epsilon)



    #Functions for the values and derivatives of the needed quantities
    def dr_dm(self,r,rho):
        return 1./(4*np.pi*r**2*rho)

    def dP_dm(self,m,r):
        return -self.G*m/(4*np.pi*r**4)

    def dL_dm(self,rho,T):
        eps = calculate_nuclear_energy(rho,T)
        return eps

    def dT_dm(self,L,r,T,rho):
        kappa = self.get_opacity(T,rho)
        return -3*kappa*L/(256*np.pi**2*self.sigma*r**4*T**3)
    
    #I include both the gaseus and radiation pressure in one method
    def P(self,rho,T):
        return rho/(self.mu*self.m_u)*self.k*T + self.a/3.*T**4

    def rho(self,P,T):
        return (P - self.a/3.*T**4)*(self.mu*self.m_u)/(self.k*T)













if __name__ == '__main__':
    #Initialization of the class
    sc = stellar_core()
    
    #Some values of the sun
    M_sun = 1.989e30
    L_sun = 3.828e26
    R_sun = 695700e3
    avg_rho = 1.408e3
	
    
    #The initial condition for the sanity check, and the ones I'll change to get a physical star
    M0 = 0.8*M_sun
    L0 = L_sun
    R0 = 0.72*R_sun
    rho0 = 5.1*avg_rho
    T0 = 5.7e6
    M_step = -1e-4*M_sun
 
    
    
    
########################################

    
    #Code for sanity test of kappa
    """
    log_T_test = [3.75,3.755,3.77,3.795,3.8]
    log_R_test = [-6.0,-5.7,-5.95,-5.55,-5.5]
    print("log T | log R | log k | k(SI) ")
    for T,R in zip(log_T_test,log_R_test):
        log_k = sc.opacity_interp2d(R,T)[-1]
        k_SI = 10**log_k*0.1
        print(" {} | {} | {:.3} | {:.3} ".format(T,R,log_k,k_SI))
    """
    
    
    
    #Code for showing the effect of different dms
    """
    dms = np.array([0.01,.1,1,10,100,1000])*M_step
    for dm in dms:
        M,R,L,rho,T,P,epsilon = sc.solve(dm,L0,R0,M0,rho0,T0,adaptive=False)
        plt.plot(M/M0,R/R_sun,label="dm=%e"%dm)
    
    plt.title("Radius vs Mass for different values of dm")
    plt.xlabel(r"$M/M_{0}$")
    plt.ylabel(r"$R/R_{\odot}$")
    plt.legend()
    plt.show()
    exit()
    """
    
    
    
    #Code for testing different initial values
    """I have only included the code for inital value for R. To see the other
    just change R to one of the other varaibles, for "Rs", the for loop and
    in the parameters of solve()"""
    """
    Rs = np.array([0.1,.5,3,5,10])*R0       
    for R0_ in Rs:
        f, axarr = plt.subplots(2, 2)
        M,R,L,rho,T,P,epsilon = sc.solve(M_step,L0,R0_,M0,rho0,T0,adaptive=False)
        axarr[0,0].plot(M/M0,R/R_sun)
        axarr[0,0].set_title("Radius")
        axarr[0,0].set_xlabel(r"$M/M_{0}$")
        axarr[0,0].set_ylabel(r"$R/R_{\odot}$")
        
        axarr[0,1].plot(M/M0,L/L_sun)
        axarr[0,1].set_title("Luminosity")
        axarr[0,1].set_xlabel(r"$M/M_{0}$")
        axarr[0,1].set_ylabel(r"$L/L_{\odot}$")
        
        axarr[1,0].plot(M/M0,T/1e6)
        axarr[1,0].set_title("Temperature")
        axarr[1,0].set_xlabel(r"$M/M_{0}$")
        axarr[1,0].set_ylabel(r"T[MK]")
        
        axarr[1,1].semilogy(M/M0,rho/avg_rho)
        #axarr[1,1].set_ylim(1,10)
        axarr[1,1].set_title("Denisity")
        axarr[1,1].set_xlabel(r"$M/M_{0}$")
        axarr[1,1].set_ylabel(r"$\log_{10}\rho/\bar{\rho}_{\odot}$")
        f.suptitle(r"Simulation for $R_0$ = %e"%R0_)
        plt.show()
        
    
    exit()
    """
    
    
########################################
        
    #Initial conditions giving different physical results.
    
    #The result presented in the report:
#    R0 *=0.5505 #at .552 begins to spike
#    rho0 *=0.375
#    T0 *=0.67

    #A collection of other solutions:
#1)        
#    R0 *=0.5475
#    rho0 *=0.379
#    T0 *=0.791
#2)  
#    R0 *=0.5495
#    rho0 *=0.676
#    T0 *=0.363
#3)     
    #R0 *=0.5495
    #rho0 *=0.679
    #T0 *=0.369

########################################



    #Check for Rho(P(rho0,T0),T0) = rho0
    assert(abs(sc.rho(sc.P(rho0,T0),T0)-rho0 < 1e-5)) 
    
    
    #Actual simulation of the star
    M,R,L,rho,T,P,epsilon = sc.solve(M_step,L0,R0,M0,rho0,T0,adaptive=True)
    
    #Printouts of the varables which we want to go to zero.
    print("M: ",M[-2]/M0)
    print("R: ",R[-2]/R0)
    print("L: ", L[-2]/L0)
    
    
    #The index where L<0.995 to mark the border of the core.
    core_border = np.argmin(np.abs(L/L0 - 0.995))


########################################  
    
    #Plotting for sanity tests
    
    plt.plot(M/M0,R/R_sun)
    plt.title("Radius")
    plt.xlabel(r"$M/M_{0}$")
    plt.ylabel(r"$R/R_{\odot}$")
    plt.show()
    plt.plot(M/M0,L/L_sun)
    plt.title("Luminosity")
    plt.xlabel(r"$M/M_{0}$")
    plt.ylabel(r"$L/L_{\odot}$")
    plt.show()
    plt.plot(M/M0,T/1e6)
    plt.title("Temperature")
    plt.xlabel(r"$M/M_{0}$")
    plt.ylabel(r"T[MK]")
    plt.show()
    plt.semilogy(M/M0,rho/avg_rho)
    plt.ylim(1,10)
    plt.title("Denisity")
    plt.xlabel(r"$M/M_{0}$")
    plt.ylabel(r"$\log_{10}\rho/\bar{\rho}_{\odot}$")
    plt.show()
    exit()
    
    

    #Plotting as a function of R, for the main simulations for the physical stars
    plt.plot(R/R0,M/M0)
    plt.title("Mass vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$M/M_0$")
    plt.show()
    plt.plot(R/R0,L/L0)
    plt.axvline(x=R[core_border]/R0,color="red",alpha=0.4,linestyle="--",label="Core Border")
    plt.title("Luminosity vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$L/L_0$")
    plt.legend()
    plt.show()
    plt.semilogy(R/R0,rho)
    plt.title("Density vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(\rho)$")
    plt.show()
    plt.plot(R/R0,T/1e6)
    plt.title("Temperature vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"T [MK]")
    plt.show()
    plt.semilogy(R/R0,P)
    plt.title("Pressure vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(P)$")
    plt.show()
    plt.semilogy(R/R0,epsilon)
    plt.title("Epsilon vs radius")
    plt.xlabel(r"$R/R_0$")
    plt.ylabel(r"$\log(\epsilon)$")
    plt.show()
