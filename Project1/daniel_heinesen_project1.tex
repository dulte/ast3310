\documentclass[a4paper,norsk, 10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage[norsk]{babel}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{float}
\usepackage{amssymb}
\usepackage[dvips]{epsfig}
\usepackage[toc,page]{appendix}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}
\usepackage{titling}
\usepackage{marvosym }
%\usepackage{subcaption}
\usepackage[noabbrev]{cleveref}
\usepackage{cite}
\usepackage{tasks}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{subfig}


\setlength{\droptitle}{-10em}   % This is your set screw

\setcounter{tocdepth}{2}

\lstset{language=c++}
\lstset{alsolanguage=[90]Fortran}
\lstset{alsolanguage=Python}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\title{AST3310 Project 1}
\author{Daniel Heinesen, daniehei}
\begin{document}
\maketitle
\section{Overview}
In this exercise we are going to simulate the core of a star, using 7 easy equations. I am first going to present these equation and the general strategy for solving them \ref{sec:decribing}. In the second part I'll look at how the different initial conditions impacts the interior of the core \ref{sec:testing}. Lastly I'll try to find the right initial values to get a star which is physical \ref{sec:simulating}. There will also be discussion of the problems I met during the process.

\section{Notes on Project0}
In the 0'th project we were asked to calculate the energy produced by the PP-chains. From this program the \verb|calculate_nuclear_energy(rho,T)| is obtained, and is used to calculate $\epsilon$ in $\frac{dL}{dm}$.



\section{Simulating the Star}
\label{sec:decribing}
\subsection{Describing the Star}
Our star is described by 10 variables, the mass $m$, the opacity $\kappa$, the energy produced by the nuclear processes $\epsilon$, the radius $r$, the density $\rho$, the temperature $T$, the luminosity $L$, the gaseous pressure $P_G$, the radiation pressure $P_{rad}$ and the total pressure $P$. The first three are said to be know: $m$ is the integration variable, $\kappa$ are read from a table and $\epsilon$ was found in \textit{Project 0}. For the remaining 7 we need equations to solve them. It can be shown that 5 such equations are \footnote{These are the 5 given in the exercise}:
\begin{equation}
\label{drdm}
\frac{\partial r}{\partial m} = \frac{1}{4\pi r^2 \rho}
\end{equation}
\begin{equation}
\label{dPdm}
\frac{\partial P}{\partial m} = -\frac{Gm}{4\pi r^4}
\end{equation}
\begin{equation}
\label{dLdm}
\frac{\partial L}{\partial m} = \epsilon
\end{equation}
\begin{equation}
\label{P}
P = P_G + P_{rad}
\end{equation}
\begin{equation}
\label{dTdm}
\frac{\partial T}{\partial m} = -\frac{3\kappa L}{256\pi^2\sigma r^4 T^3},
\end{equation}
where $G = 6.674\cdot10^{-11} m^3\cdot kg^{-1}\cdot s^{-2}$ is the gravitational constant and $\sigma = 5.6704\cdot 10^{-8} W\cdot m^{-2}\cdot K^{-4}$ the Stephan-Boltzmann constant.

The general rule is that we need 7 equations to solve 7 unknowns, so we need 2 more. We can take the core of the star to behave like a ideal gas, and we can therefore use the ideal gas law:
\begin{equation}
P_G V = Nk_B T,
\end{equation}
where $V$ is the volume, $N$ the number of particles and $k_B = 1.38065 \cdot 10^{-23} J\cdot K^{-1}$. We do not have $N$ nor $V$, but can rewrite the equation
\begin{equation}
\frac{N}{V} = \frac{N\rho}{m} = \frac{\rho}{m_u \mu}.
\end{equation}
In the last step we use that $N/m = 1/m_u \mu$ is the mean weight of a particle in the star. $m_u = 1.66054\cdot 10^{-27}$kg is atomic mass constant and is the typical weight of a nucleon. $\mu$ is the average molecular weight. How to calculate this will be described below \ref{sec:mu}. This gives us
\begin{equation}
\label{PG}
P_G = \frac{\rho}{m_u \mu}k_B T.
\end{equation}

For the last equation we simply use the equation for the radiation pressure $P_{rad}$
\begin{equation}
\label{Prad}
P_{rad} = \frac{a}{3}T^4,
\end{equation}
where $a = 7.5657\cdot 10^{-16} J\cdot m^{-3} K^{-4}$ is the radiation constant. We now have almost everything we need to simulate the star core. But before we can look at the solver algorithm we need $\mu$

\subsection{Calculation of $\mu$}
\label{sec:mu}
A mentioned above, $\mu$ is the unitless average molecular weight. This can be seen as the fraction of atomic mass unit a typical particle has. To find this we need to see what fraction of different particles we have. We let $X$ be the fraction of hydrogen, $Y$ the fraction of helium and $Z$ the fraction of metal \footnote{Anything heavier than helium.}. The average particle mass can then be shown to have the form:

\begin{equation}
\mu = \frac{1}{(1+I_X)X + (1+I_Y)Y/4 + Z/2}
\end{equation}

Where $I_X$ and $I_Y$ is the fraction of hydrogen and helium which is ionized\footnote{We always take the metals to be fully ionized. Then it can be show that we can just use $Z/2$}. We are in these simulation assuming that both hydrogen and helium is fully ionized, and we therefore get

\begin{equation}
\label{mu}
\mu = \frac{1}{2X + 2Y/4 + Z/2}
\end{equation}

\subsection{Notes on $\kappa$}
We have to be careful with the interpolated function from the $\kappa$ file, since the file has $\log \kappa$ in \textit{cgs} units. Therefore $\log T$ and $\log R$ needs to be converted from SI to cgs. $T$ is the same in both units, so we only need to look at $R$. Since
\begin{equation}
R = \frac{\rho}{\left( \frac{T}{10^{6}}\right)^3}
\end{equation}
$\rho$ has to be converted to cgs by simply as $\rho \rightarrow \rho\cdot 0.001$. Having retrieved $\log \kappa$ and taken $\kappa = 10^{\log \kappa}$, we simply returns this value in SI by the simple conversion $\kappa \rightarrow \kappa \cdot 0.1$.

\subsection{Notes on the adaptive steps}
To control the integration for the variable where the change is great, we use adaptive step length. We choose a $p$ such that relative difference during one step is

\begin{equation}
\frac{dV}{V} < p
\end{equation}

and with

\begin{equation}
\frac{dV}{dm} = f 
\end{equation}
we can show that 
\begin{equation}
dm = \frac{dV}{f} = \frac{pV}{f},
\end{equation}
for some variable $V$. We will choose the smallest of these $dm$s. For our simulation $p=10^{-4}$.

\subsection{Solving the Equations}
The equations \eqref{drdm}, \eqref{dPdm}, \eqref{dLdm} and \eqref{dTdm} are (coupled) differential equations. To solve these numerically we need some kind of solving scheme. For simplicity we are going to use a normal Euler scheme
\begin{equation}
V_{i+1} = V_{i} + \frac{\partial V}{\partial m}\cdot dm,
\end{equation}
where $V$ is some variable. While Euler only is a first-order scheme, our functions are sufficiently smooth and non-periodic so that with a small $dm$, we should get a quite precise solution. But, since we are using a first-order scheme, we will need to use an adaptive step length at the very end of the simulation.

We can now set up an algorithm for solving the equations:
\begin{enumerate}
\item Initialize arrays/lists with $m_0$, $r_0$, $L_0$, $T_0$ and $\rho_0$, and some step $dm$.
\item Use $\rho_0$ to calculate $P_0$
\item while $m \geq 0.05\cdot m_0$
\begin{enumerate}[i]
\item Calculate $V_{i+1} = V_{i} + \frac{\partial V}{\partial m}\cdot dm$ for each variable.
\item Use the updated $P$ to calculate $\rho_{i+1}$ from $P_G = P - P_{rad}$ and $T$.
\item Update the mass $m_{i+1} = m_i + dm$
\item If adaptive steps enabled: Check if a change in step length is needed and update $dm$.
\item If $dm$ becomes so small that the program take unnecessary long time, adaptive steps are turned off. This is normally set to around $dm < 10^{20}$
\item If $m$, $L$ or $T$ is less than 0, return the list without the last element. 
\end{enumerate}
\item Return lists and process the data.
\end{enumerate}

One note on the algorithm:
\\

When $V_{i+1} = V_{i} + \frac{\partial V}{\partial m}\cdot dm$ is calculated, $V_{i+1}$ is appended to the lists in my code. We have to be careful when calculating some of these expressions. $T$ is the last to be updated, but is dependent on $L$ and $r$ in the previous step. Since we are using python lists $T$ has to be updated with the second to last element of $L$ and $r$, since these are the values at the previous step.
\\


With this algorithm we should be able to solve the 7 equations, getting a model of the core of a star. 


\subsection{Problems During Simulation}
There were few problems during the simulation part. Some parenthesise error took an hour to debug, but other than that, the coding part went smooth. The adaptive step length was somewhat difficult to wrap my head around, but after some 5-6 readthroughs and some help from other students, it became understandable.

\section{Testing the Program}
\label{sec:testing}
\subsection{Benchmarking/Sanity Tests}
A couple of test could be done to benchmark the solver. The first one was a test that $\rho(P(\rho_0,T_0),T_0)$ is the same as $\rho_0$. In the program this is done right before the solver function is called, by a python \verb|assert|, which checks if this equality is satisfied up to a small number $\epsilon \approx 10^{-5}$. This check has yet to fail.

The next test is to see if the calculated values of $\kappa$ is correct:

\begin{table}[H]
\centering
\begin{tabular}{c|c|c|c|c|c}
$\log_{10} T$ & $\log_{10} R$ & $\log_{10} \kappa $(cgs) calculated & $\log_{10} \kappa$ (cgs) sanity test & $\kappa$ (SI) calculated & $\kappa$ (SI) sanity test \\
\hline  
$3.750$ & $-6.00$ & $-1.55$ & $-1.55$ & $0.00284$ & $0.00284$ \\
$3.755$ & $-5.70$ &$-1.61$&$-1.61$&$0.00245$&$0.00246$\\
$3.770$ & $-5.95$ & $-1.33$ & $-1.33$ & $0.00472$ & $0.00470$\\
$3.795$ & $-5.55$ & $-1.16$ & $-1.16$ & $0.00689$ & $0.00689$\\
$3.800$ & $-5.50$ & $-1.11$ & $-1.11$ & $0.00769$ & $0.00769$
\end{tabular}
\caption{The sanity check for $\kappa$. We can see that the calculated values are close to the sanity check, thus we can move on.}
\end{table}

The last and maybe most telling sanity check is the test of the simulation with certain initial values:

\begin{equation}
M_0 = 0.8\cdot M_{\odot}
\end{equation}
\begin{equation}
R_0 = 0.72\cdot R_{\odot}
\end{equation}
\begin{equation}
L_0 = 1.0\cdot L_{\odot}
\end{equation}
\begin{equation}
T_0 = 5.7\cdot 10^{6} K
\end{equation}
\begin{equation}
\rho_0 = 5.1\cdot \bar{\rho}_{\odot}
\end{equation}
where $\bar{\rho}_{\odot} =1.408\cdot10^3 kg m^{-3}$, and using a $dm = -4\cdot 10^{-4}\cdot M_{\odot}$. This gives

\begin{figure}[H]
   \centering
   \subfloat[][]{\includegraphics[width=.4\textwidth]{images/R_san.png}}\quad
   \subfloat[][]{\includegraphics[width=.4\textwidth]{images/L_san.png}}\\
   \subfloat[][]{\includegraphics[width=.4\textwidth]{images/T_san.png}}\quad
   \subfloat[][]{\includegraphics[width=.4\textwidth]{images/rho_san.png}}
   \caption{Plots showing the simulations for the initial conditions constituting the sanity test.}
   \label{fig:sanity_test}
\end{figure}

We can see from fig. \ref{fig:sanity_test} that the results from the simulations are quite close to the ones in the sanity check. The only problem is that my simulation stops a bit short of the sanity checks. Example: The radius stop right below 0.62, while the sanity check stops below 0.6. After talking to Charalambos we concluded that this was to be expected. The precise initial conditions of the sanity check was somewhat uncertain ($M/M_{sun}$ should not start at 1 with the given initial conditions). Also, the value of $\mu$ also makes a 'large' differences in the results, and since this was not given, this can be the reason for the inconsistencies.

Other than that, the sanity checks seems to give the correct answer, and we are ready to move on.

\subsection{Changing $dm$}
Up till now we have used $dm_0 = -4\cdot 10^{-4}\cdot M_0$, but what effect does changing this value have on the simulation. To check this we can do the same simulation as above, but for values in the range $0.01-1000$ times the original $dm_0$. The result can be found in fig. \ref{fig:different_dm}. We can see that for $0.01-100$ time $dm_0$ there are very small differences, but for $1000\cdot dm_0$ we begin to see some difference. The fact that the radii is more or less the same for the lower values of $dm$s, and seem to converge, means that we probably can use the initial value for $dm = -4\cdot 10^{-4}\cdot M_0 $ in this regime and get a quite accurate result. But, I use '\textit{in this regime}' for a reason. In the start, most of the variables behave \textit{nice}, meaning that their change is small, and that we can use this $dm$. For the radius and luminosity, especially, the functions tend to decrease drastically near $0$, so here the $dm$ will be to large. This is the reason why we use adaptive step length. But the adaptive step length is only typically needed for $m<0.05\cdot M_0$, so for the values of $r$ and $m$ in fig. \ref{fig:different_dm}, $dm_0$ is sufficient.

\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.45\textwidth]{images/diff_dr.png}}\quad
   \subfloat[][]{\includegraphics[width=.45\textwidth]{images/diff_dr_zoom.png}}\\
   \caption{Plots showing $r$ vs $m$ for different values of $dm$. $a)$ shows the whole simulation, and we can see that only the largest values of $dm$ makes a noticeable different. $b)$ show a zoomed in version at the very end of the simulation. Here we can see that the lowest values of $dm$ gives more or less the same values for $r$. The reason some of the simulation are closer to 0 than others is that the value of $dm$ determines how close $r$ gets to $0$, before the simulation is ended, due to $m$ being less than 0.}
   \label{fig:different_dm}
\end{figure}


\subsection{Playing with the initial conditions}
We now want to see what the different initial values do to the simulation. We are going to change one of the initial parameters $r_0$, $T_0$, $\rho_0$ and $P_0$ at the time, with a factor in the set $\{0.1,0.5,3,5\}$. The exercise calls for quite many plots, since we are limited to 10 pages, I had to make the plot small. There will be a total of 16 plots here, so I am going to group them four-by-four, one for each changed initial parameter, to save space. This means that the plots are quite small, but since PDF uses vector graphics, one should be able to zoom in with good resolution. I made the plot as big as possible before they began stacking one under the other. If I increase their size by some $0.01$ times the text width the length of the report would become $\sim 6$ pages longer. If this exercise is printed out before reading, I cant say anything other than sorry.

\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_r_1.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_r_2.png}}\\
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_r_3.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_r_4.png}}
   \caption{Plots showing the simulations for different initial values for $r$. The plots are in order of increasing $r_0$}
   \label{fig:init_r}
\end{figure}


\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_T_1.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_T_2.png}}\\
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_T_3.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_T_4.png}}
   \caption{Plots showing the simulations for different initial values for $T$. The plots are in order of increasing $T_0$}
   \label{fig:init_T}
\end{figure}

\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_rho_1.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_rho_2.png}}\\
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_rho_3.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_rho_4.png}}
   \caption{Plots showing the simulations for different initial values for $\rho$. The plots are in order of increasing $\rho_0$}
   \label{fig:init_rho}
\end{figure}

\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_P_1.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_P_2.png}}\\
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_P_3.png}}\quad
   \subfloat[][]{\includegraphics[width=.48\textwidth]{images/init_P_4.png}}
   \caption{Plots showing the simulations for different initial values for $P$. The plots are in order of increasing $P_0$}
   \label{fig:init_P}
\end{figure}

We can see from fig. \ref{fig:init_r} that for smaller values of $r_0$ the luminosity falls to $0$ faster than for the original $r_0$, and for $r_0 \rightarrow 0.1r_0$ the luminosity actually hits $0$. For the larger values of $r_0$ we see that the radius becomes so large, that not much happens with $T$, $L$ and $\rho$ before the mass runs out.

From fig. \ref{fig:init_T} we see that for $T_0 \rightarrow 0.1T_0$ we have both $r$ and $L$ fall to almost $0$, while $T$ and $\rho$ have some very strange behaviour at the start of the simulation, with what looks almost like a discontinuity. What the reason for this is, is uncertain, but looks not non-physical. For larger $T_0$'s we see that the radius is decreasing more like the original $T_0$, but the luminosity still goes more towards $0$. Thus it seems that one can easily control the luminosity with the initial value of $T$.

I have included both fig. \ref{fig:init_P} and fig. \ref{fig:init_rho} mostly to state a point: Since $P$ is a function of $\rho$ and $T$ alone (and $\rho$ of $P$ and $T$), and calculated from one another, whether one choose $P_0$ or $\rho_0$, the same results will be obtained. This is why we only will use $\rho_0$ when finding the physical solution. For this part of the discussion I am only going to talk about $P_0$, since it has the same effect to that of $\rho_0$. We see that the small initial pressure $P_0 \rightarrow 0.1P_0$ has an immense effect on the simulation. Both $r$ and $L$ goes towards $0$, while $T$ and $\rho$ increases rapidly near $m=0$. We will see this again later. For $0.5P_0$ this effect is mostly gone, and for the larger $P_0$s the results are more like those of the original $P_0$.
We see that this is quite like the results we got for $T_0$. So changing $T_0$ and $P_0$( and $\rho_0$) seems to have the same effect on the simulation.


\section{Finding the physical solutions}
\label{sec:simulating}
We have now come to the concluding part of the exercise, find physical solutions for the core of the star. To find this we want 2 things to happen: $T$, $L$ and $m$ need to go to 0 at the same time. This is difficult, so we instead want them to reach 5\% of there initial values of the same time (or better). The other goal is to have the core $L<0.995\cdot L_0$ go to at least 10\% of $r_0$.
There are many smart methods for achieving this. My first thought was some psudo-Monte-Carlo simulation without any real probability distribution\footnote{There a good reason I scrapped that idea...}. I ended up searching manually. Knowing that increasing/decreasing $\rho_0$ and $T_0$ had similar effects on the simulation, it was easy to find multiple solutions once one was found. 

Holding $m_0$ and $L_0$ constant (as stated in the exercise) I began with $R_0 \approx 0.5\cdot R_{0,exercise}$, and $T_0$ and $\rho_0$ about the same. After an hour searching I had 3-4 different solution. I am going to present two of them here, with the only minuscule difference between them being $R_0$:

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
Initial Conditions & Endpoint without adaptive & Endpoint with adaptive \\
\hline
$r_0 = 0.5505\cdot M_{0,exercise}$ & $m = 0.0053\cdot m_0$ & $m = 0.00482\cdot m_0$ \\
$\rho_0 = 0.375\cdot \rho_{0,exercise}$ & $r = 0.047\cdot r_0$ & $r = 0.034\cdot r_0$ \\
$T_0 = 0.67\cdot T_{0,exercise}$ & $L = 0.002\cdot L_0$ & $L = 8.32\cdot 10^{-10}\cdot L_0$ 
\end{tabular}
\caption{A solution for a physical star. We can see that without adaptive steps we get $r$ and $L$ around 5\% of there respective initial condition. With adaptive steps this result becomes better, with $r$ reaching almost $3\%$ of its initial condition, and $L$ goes more or less to 0.}
\label{tab:sol}
\end{table}

\begin{table}[H]
\centering
\begin{tabular}{c|c|c}
Initial Conditions & Endpoint without adaptive & Endpoint with adaptive \\
\hline
$r_0 = 0.5520\cdot M_{0,exercise}$ & $m = 0.00486\cdot m_0$ & $m = 0.00501\cdot m_0$ \\
$\rho_0 = 0.375\cdot \rho_{0,exercise}$ & $r = 0.0169\cdot r_0$ & $r = 0.00085\cdot r_0$ \\
$T_0 = 0.67\cdot T_{0,exercise}$ & $L = 0.012\cdot L_0$ & $L = 2.47\cdot 10^{-6}\cdot L_0$ 
\end{tabular}
\caption{The only difference from tab. \ref{tab:sol} is that $r_0 = 0.5520\cdot M_{0,exercise}$ now, this is a small change, but has huge effects. With adaptive steps $r$ has decreased 100-fold, but while $L$ has increased some, it is still more or less $0$. The real difference are not in the tables.}
\label{tab:sol2}
\end{table}

If we look at table \ref{tab:sol} and \ref{tab:sol2} we see that we have solutions that have fulfilled the first criteria. We will see from the plots that they fulfil the last one as well. The interesting difference between them are seen in the plots

\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/m_small_r0.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/L_small_r0.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/T_small_r0.png}}\\
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/eps_small_r0.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/rho_small_r0.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/P_small_r0.png}}
   \caption{We finally have some plots for a physical solution. As $m\rightarrow 0$ both $m$ and $L$ does the same. If we look at $(b)$ we see the plot of the luminosity. The red dashed line indicates the core border where $L=0.995\cdot L_0$. We see that the core ends at around $0.6\cdot r_0$, which is way within our goal of at least $0.1\cdot r_0$. We can see that $\rho$, $P$ and $\epsilon$ follows more or less the same shape (helped by the logarithm). They, together with $T$, increase as we go towards the center of the core, as is expected.}
   \label{fig:sol}
\end{figure}


\begin{figure}[!ht]
   \centering
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/m.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/L.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/T.png}}\\
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/eps.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/rho.png}}\quad
   \subfloat[][]{\includegraphics[width=.3\textwidth]{images/P.png}}
   \caption{We see that we get almost the same behaviour here as in fig. \ref{fig:sol}, but there is a striking difference close to $m = 0$: The sudden increase in $T$, $\rho$, $P$ and $\epsilon$. This is discussed more in the text.}
   \label{fig:sol2}
\end{figure}

As mentioned in fig. \ref{fig:sol2} we see some strange behaviour near $m=0$, that looks like the one in fig. \ref{fig:init_P} and fig. \ref{fig:init_rho}. Sometime after $m<0.05\cdot m_0$ the luminosity suddenly drops to $0$ as $T$, $\rho$, $P$ and $\epsilon$ increase drastically. My main two hypotheses are:

\begin{enumerate}
\item There are some numerical error when $r$ and $L$ come to close to $0$, which makes the other variables blow up. Since we are using adaptive steps this should take care of this. When adaptive steps are stopped at $dm = -10^{20}$ to save time, there may be problems close to $0$, but even with the lower bound of $dm$ set to $-10^{10}$, the same effects occur. This may indicate that the effect is physical.
\item The physical explanation may be that at a certain point the density and temperature is so large that the PP-II chain kicks in ($\sim 14$ MK), and then PP-III starts at  $\sim 23$ MK, producing even more energy. This would lead to a sharp decrease in luminosity and a rise in the temperature, which again would lead to an increase in pressure and density. 
\end{enumerate}

As of now, the second hypothesis seems most likely. This could be checked by looking at the rates of the PP-chain, but it would be difficult to find the causal order of the events \footnote{And poor time management make it difficult to rewrite project0 to make such a plot possible.}

\subsection{Problems During the simulations}
There were few problems during the simulations. Of course it took some time to find the physical solutions, by after some time I got the hang of it, and finding a new solution took little time. All in all there were few problems with the whole project, other then some stupid parenthesis and $\pm$ errors in the code.

\section{Conclusion}
We have now seen at a simulation of a star core. We saw that varying $dm$ gave some, but not much difference, justifying us sticking with the one from the exercise. We then saw that a change in $T_0$ and $P_0$(and $\rho_0$) changed the simulation in similar ways. Meaning that if one solution is found, switching $\rho_0$ and $T_0$ would lead to another physical solution -- or something close to it. Finally we found two similar physical solutions. For one of them $T$, $\rho$, $P$ and $\epsilon$ blew up close to $m=0$, which may have be caused by, and contributed to the start of the  PP-II and PP-III chains.

\section{The Code}
The code found in \verb|project0.py| and \verb|project1.py| were mostly written and run on a windows machine in python 3.5. The code has been tested on a ubuntu laptop.

\section{Collaboration}
For this exercise I have discussed, helped and been helped by some lovely fellow students. A big thanks goes out to last years student \textit{Julie Thingwall} for some help with the adaptive steps and the report. From my year,	 \textit{Sofie Magdalena} and \textit{Aram Salihi} has be great discussion partners for this exercise.

\end{document}

